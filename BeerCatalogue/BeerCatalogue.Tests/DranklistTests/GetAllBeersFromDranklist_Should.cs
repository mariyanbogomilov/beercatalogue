﻿using System.Linq;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerCatalogue.Tests.DranklistTests
{
    [TestClass]
    public class GetAllBeersFromDranklist_Should
    {
        [TestMethod]
        public void GetAllBeersFromUserDranklist_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(GetAllBeersFromUserDranklist_Correctly));

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer1",
            };

            var secondBeer = new Beer
            {
                Id = 2,
                Name = "Kamenitza",
                ABV = "4.7%",
                Description = "TestBeer2",
            };

            var thirdBeer = new Beer
            {
                Id = 3,
                Name = "Ariana",
                ABV = "4.5%",
                Description = "TestBeer3",
            };

            var dranklist = new Dranklist
            {
                BeerId = 1,
                UserId = 1
            };

            var secondDranklist = new Dranklist
            {
                BeerId = 3,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.Beers.Add(secondBeer);
                arrangeContext.Beers.Add(thirdBeer);
                arrangeContext.Dranklists.Add(dranklist);
                arrangeContext.Dranklists.Add(secondDranklist);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                var result = sut.GetAllBeers(user.UserName);

                Assert.AreEqual(dranklist.BeerId, result.Result.ToArray()[0].Id);
                Assert.AreEqual(secondDranklist.BeerId, result.Result.ToArray()[1].Id);
            }
        }
    }
}
