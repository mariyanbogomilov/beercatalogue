﻿using System;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerCatalogue.Tests.DranklistTests
{
    [TestClass]
    public class CreateDranklist_Should
    {
        [TestMethod]
        public void ReturnCorrectDranklist_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectDranklist_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var dranklistDto = new DranklistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                var result = sut.Create(dranklistDto);

                Assert.AreEqual(dranklistDto.UserId, result.Result.UserId);
                Assert.AreEqual(dranklistDto.BeerId, result.Result.BeerId);
            }
        }

        [TestMethod]
        public void Throw_When_DranklistBeerDoesNotExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_DranklistBeerDoesNotExists));

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var dranklistDto = new DranklistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Users.Add(user);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(dranklistDto));
            }
        }

        [TestMethod]
        public void Throw_When_DranklistUserDoesNotExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_DranklistUserDoesNotExists));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var dranklistDto = new DranklistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Beers.Add(beer);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(dranklistDto));
            }
        }

        [TestMethod]
        public void Throw_When_DranklistAlreadyExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_DranklistAlreadyExist));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var dranklist = new Dranklist
            {
                BeerId = 1,
                UserId = 1
            };

            var dranklistDto = new DranklistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.Dranklists.Add(dranklist);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(dranklistDto));
            }
        }

    }
}
