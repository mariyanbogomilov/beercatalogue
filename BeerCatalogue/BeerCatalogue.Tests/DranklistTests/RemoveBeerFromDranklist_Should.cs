﻿using System.Linq;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerCatalogue.Tests.DranklistTests
{
    [TestClass]
    public class RemoveBeerFromDranklist_Should
    {
        [TestMethod]
        public void CorrectlyRemoveBeerFromDranklist_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(CorrectlyRemoveBeerFromDranklist_When_ParamsAreValid));

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
            };

            var secondBeer = new Beer
            {
                Id = 2,
                Name = "Kamenitza",
                ABV = "4.7%",
                Description = "TestBeer2",
            };

            var dranklist = new Dranklist
            {
                BeerId = 1,
                UserId = 1
            };

            var secondDranklist = new Dranklist
            {
                BeerId = 2,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.Beers.Add(secondBeer);
                arrangeContext.Dranklists.Add(dranklist);
                arrangeContext.Dranklists.Add(secondDranklist);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                var result = sut.Remove(1, user.UserName);

                Assert.IsTrue(result.Result);
                Assert.AreEqual(1, assertContext.Dranklists.Count());
                Assert.AreEqual(1, assertContext.Users.First(u => u.UserName == user.UserName).Dranklist.Count());
            }

        }

        [TestMethod]
        public void ReturnFalse_When_ParamsAreInvalid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnFalse_When_ParamsAreInvalid));

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new DranklistService(assertContext);

                var result = sut.Remove(2, "Gosho");

                Assert.IsFalse(result.Result);
            }
        }
    }
}
