﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.ReviewTests
{
    [TestClass]
    public class DeleteReview_Should
    {
        [TestMethod]
        public void Succsesfull_Delete_ReviewList()
        {
            var options = BeerUtility.GetOptions(nameof(Succsesfull_Delete_ReviewList));
            var date = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 22,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };
            var user = new User
            {
                Id = 22,
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };
            var wish = new ReviewDTO
            {
                BeerName = beer.Name,
                Username = user.UserName,
                Comment = "Very good beer"
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object,assertContext);

                var create = sut.Create(wish);
                var result = sut.Delete(beer.Id,user.Id);

                var wishList = assertContext.Users.First();
                var actualCount = wishList.Reviews.Count;
                Assert.AreEqual(0, actualCount);
                Assert.IsTrue(result.Result);
            }
        }

        [TestMethod]
        public void GetCorrectMessage_When_WishListCannotBeDeleted()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectMessage_When_WishListCannotBeDeleted));
            var date = new Mock<IDateTimeProvider>();
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object,assertContext);

                var result = sut.Delete(20,21);

                Assert.IsFalse(result.Result);
            }
        }
    }
}
