﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.ReviewTests
{
    [TestClass]
    public class CreateReview_Should
    {
        [TestMethod]
        public void Succsesfully_AddReviw_ToBeer()
        {
            var options = BeerUtility.GetOptions(nameof(Succsesfully_AddReviw_ToBeer));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };
            var user = new User
            {
                Id = 1,
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var review = new ReviewDTO
            {
                BeerName = beer.Name,
                Username = user.UserName,
                Comment = "Very good beer"
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object,assertContext);
                
                var result = sut.Create(review);

                var actual = result.Result;
                var userA = assertContext.Beers.First();
                var count = userA.Reviews.Count;

                Assert.AreEqual(1, count);
            }
        }


        [TestMethod]
        public void Throw_Exception_When_UserIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_Exception_When_UserIsNotFound));
            var date = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 111,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };

            var review = new ReviewDTO
            {
                BeerId = beer.Id,
                UserId = 5,
                Comment = "Mnogo dobre bira"
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object,assertContext);

                var result = sut.Create(review);

                var actual = result.Exception.Message;
                var expected = "One or more errors occurred. (Beer or user does not exist!)";

                Assert.AreEqual(expected, actual);
            }
        }
        [TestMethod]
        public void Throw_Exception_When_BeerIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_Exception_When_BeerIsNotFound));
            var date = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var user = new User
            {
                Id = 111,
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var review = new ReviewDTO
            {
                BeerId = 212,
                UserId = user.Id,
                Comment = "Mnogo dobre bira"
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object, assertContext);

                var result = sut.Create(review);

                var actual = result.Exception.Message;
                var expected = "One or more errors occurred. (Beer or user does not exist!)";

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
