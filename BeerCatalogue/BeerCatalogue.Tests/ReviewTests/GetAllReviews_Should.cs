﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.ReviewTests
{
    [TestClass]
    public class GetAllReviews_Should
    {
        [TestMethod]
        public void Succsesfully_GetReviws_ToBeer()
        {
            var options = BeerUtility.GetOptions(nameof(Succsesfully_GetReviws_ToBeer));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };
            var user = new User
            {
                Id = 1,
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var review = new ReviewDTO
            {
                BeerName = beer.Name,
                Username = user.UserName,
                Comment = "Very good beer"
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new ReviewService(date.Object, assertContext);

                var create = sut.Create(review);

                var actual = sut.GetAllReviews(user.Id).Result;
;
                var result = actual.First();

                Assert.AreEqual(review.Comment, result.Comment);
                Assert.AreEqual(1, actual.Count());
            }
        }
    }
}
