﻿using System.Linq;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BreweryTests
{
    [TestClass]
    public class GetBreweriesForCountry_Should
    {
        [TestMethod]
        public void ReturnCorrectBreweriesForCountry_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectBreweriesForCountry_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"
            };

            var secondCountry = new Country
            {
                Id = 2,
                Name = "Czech"
            };

            var firstBrewery = new Brewery
            {
                Id = 1,
                Name = "Kamenitza",
                CountryId = 1
            };

            var secondBrewery = new Brewery
            {
                Id = 2,
                Name = "Pirinsko",
                CountryId = 1
            };

            var thirdBrewery = new Brewery
            {
                Id = 3,
                Name = "Staropramen",
                CountryId = 2
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(firstBrewery);
                arrangeContext.Breweries.Add(secondBrewery);
                arrangeContext.Breweries.Add(thirdBrewery);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                var result = sut.GetBreweriesForCountry(1);

                Assert.AreEqual(2, result.Result.Count());
            }
        }
    }
}
