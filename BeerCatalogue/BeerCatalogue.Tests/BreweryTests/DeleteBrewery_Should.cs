﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BreweryTests
{
    [TestClass]
    public class DeleteBrewery_Should
    {
        [TestMethod]
        public void DeleteBrewery_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(DeleteBrewery_Correctly));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"
            };

            var firstBrewery = new Brewery
            {
                Id = 1,
                Name = "Kamenitza",
                CountryId = 1
            };

            var secondBrewery = new Brewery
            {
                Id = 2,
                Name = "Pirinsko",
                CountryId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(firstBrewery);
                arrangeContext.Breweries.Add(secondBrewery);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);
                
                Assert.IsTrue(sut.Delete(2).Result);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_BreweryDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnFalse_When_BreweryDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                Assert.IsFalse(sut.Delete(2).Result);
            }
        }
    }
}
