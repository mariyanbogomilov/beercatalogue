﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.BreweryTests
{
    [TestClass]
    public class CreateBrewery_Should
    {
        [TestMethod]
        public void ReturnCorrectBrewery_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectBrewery_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var breweryDto = new BreweryDTO
            {
                Id = 1,
                Name = "Staropramen",
                CountryId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                var result = sut.Create(breweryDto);

                Assert.AreEqual(breweryDto.Id, result.Result.Id);
                Assert.AreEqual(breweryDto.Name, result.Result.Name);
                Assert.AreEqual(breweryDto.CountryId, result.Result.CountryId);
            }
        }

        [TestMethod]
        public void Throw_When_BreweryAlreadyExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_BreweryAlreadyExists));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "Staropramen",
                CountryId = 1
            };

            var breweryDto = new BreweryDTO
            {
                Id = 1,
                Name = "Staropramen",
                CountryId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(breweryDto));
            }
        }
    }
}
