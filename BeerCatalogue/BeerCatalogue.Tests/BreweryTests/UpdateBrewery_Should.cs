﻿using System;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BreweryTests
{
    [TestClass]
    public class UpdateBrewery_Should
    {
        [TestMethod]
        public void CorrectlyUpdateBrewery_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(CorrectlyUpdateBrewery_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria"
            };

            var secondCountry = new Country
            {
                Id = 2,
                Name = "Czech"
            };

            var firstBrewery = new Brewery
            {
                Id = 1,
                Name = "Kamenitza",
                CountryId = 1
            };

            var secondBrewery = new Brewery
            {
                Id = 2,
                Name = "Pirinsko",
                CountryId = 1
            };

            var breweryDto = new BreweryDTO
            {
                Name = "Ariana",
                CountryId = 2
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Countries.Add(secondCountry);
                arrangeContext.Breweries.Add(firstBrewery);
                arrangeContext.Breweries.Add(secondBrewery);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                var result = sut.Update(2, breweryDto);

                Assert.AreEqual(breweryDto.Name, result.Result.Name);
                Assert.AreEqual(breweryDto.CountryId, result.Result.CountryId);
            }
        }

        [TestMethod]
        public void Throw_When_BreweryDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_BreweryDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            var breweryDto = new BreweryDTO
            {
                Id = 1,
                Name = "Ariana",
                CountryId = 2
            };

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Update(2, breweryDto));
            }
        }
    }
}
