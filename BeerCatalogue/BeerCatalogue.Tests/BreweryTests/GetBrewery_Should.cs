﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.BreweryTests
{
    [TestClass]
    public class GetBrewery_Should
    {
        [TestMethod]
        public void GetCorrectBrewery_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectBrewery_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "Staropramen",
                CountryId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                var result = sut.GetBrewery(1);

                Assert.AreEqual(brewery.Id, result.Result.Id);
                Assert.AreEqual(brewery.Name, result.Result.Name);
                Assert.AreEqual(brewery.CountryId, result.Result.CountryId);
            }
        }

        [TestMethod]
        public void Throw_When_BreweryDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_BreweryDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BreweryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBrewery(1));
            }
        }
    }
}
