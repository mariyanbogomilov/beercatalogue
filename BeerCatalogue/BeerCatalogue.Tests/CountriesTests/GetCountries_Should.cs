﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.CountriesTests
{
    [TestClass]
    public class GetCountries_Should
    {
        [TestMethod]
        public void ReturnCountries_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCountries_Correctly));

            var dateTime = new Mock<IDateTimeProvider>();

            var firstCountry = new Country
            {
                Id = 1,
                Name = "Russia"
            };

            var secondCountry = new Country
            {
                Id = 2,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(firstCountry);
                arrangeContext.Countries.Add(secondCountry);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                var result = sut.GetAllCountries();

                Assert.AreEqual(2, result.Result.Count());
                Assert.AreEqual(2, result.Result.ToArray()[1].Id);
                Assert.AreEqual(1, result.Result.ToArray()[0].Id);
            }
        }
    }
}
