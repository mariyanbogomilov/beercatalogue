﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.CountriesTests
{
    [TestClass]
    public class GetCountry_Should
    {
        [TestMethod]
        public void ReturnCorrectCountry()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectCountry));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Russia"
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                var act = sut.GetCountry(1);

                var result = act.Result;

                Assert.AreEqual(country.Id, result.Id);
                Assert.AreEqual(country.Name, result.Name);
            }
        }

        [TestMethod]
        public void Throw_When_CountryDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_CountryDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            using(var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetCountry(1));
            }
        }
    }
}
