﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.CountriesTests
{
    [TestClass]
    public class UpdateCountry_Should
    {
        [TestMethod]
        public void CorrectlyEditCountry_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(CorrectlyEditCountry_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Russia"
            };

            var countryDto = new CountryDTO
            {
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                var act = sut.Update(1, countryDto);

                var result = act.Result;

                Assert.AreEqual(countryDto.Name, result.Name);
            }
        }

        [TestMethod]
        public void Throw_When_CountryToUpdateDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_CountryToUpdateDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            var countryDto = new CountryDTO
            {
                Id = 2,
                Name = "Bulgaria"
            };

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Update(1, countryDto));
            }
        }
    }
}
