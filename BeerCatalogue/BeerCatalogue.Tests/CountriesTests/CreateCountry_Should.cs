﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.CountriesTests
{
    [TestClass]
    public class CreateCountry_Should
    {
        [TestMethod]
        public void ReturnCorrectCountry_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectCountry_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var countryDto = new CountryDTO
            {
                Id = 1,
                Name = "Russia"
            };

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                var act = sut.Create(countryDto);

                var actual = act.Result;

                Assert.AreEqual(countryDto.Id, actual.Id);
                Assert.AreEqual(countryDto.Name, actual.Name);
            }
        }

        [TestMethod]
        public void Throw_When_CountryAlreadyExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_CountryAlreadyExists));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Russia"
            };

            var countryDto = new CountryDTO
            {
                Id = 1,
                Name = "Russia"
            };

            using(var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(countryDto));
            }
        }
    }
}
