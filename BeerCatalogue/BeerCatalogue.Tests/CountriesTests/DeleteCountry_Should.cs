﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.CountriesTests
{
    [TestClass]
    public class DeleteCountry_Should
    {
        [TestMethod]
        public void ReturnTrue_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));

            var dateTime = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                Id = 1,
                Name = "Russia"
            };

            using (BeerCatalogueContext arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                Assert.IsTrue(sut.Delete(1).Result);
            }   
        }

        [TestMethod]
        public void ReturnFalse_When_CountryToDeleteDoesNotExist()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnFalse_When_CountryToDeleteDoesNotExist));

            var dateTime = new Mock<IDateTimeProvider>();

            using (BeerCatalogueContext assertContext = new BeerCatalogueContext(options))
            {
                var sut = new CountryService(dateTime.Object, assertContext);

                Assert.IsFalse(sut.Delete(1).Result);
            }
        }
    }
}
