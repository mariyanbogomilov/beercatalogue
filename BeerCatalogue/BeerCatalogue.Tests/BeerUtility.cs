using Microsoft.EntityFrameworkCore;
using BeerCatalogue.Database;

namespace BeerCatalogue.Tests
{
    public class BeerUtility
    {
        public static DbContextOptions<BeerCatalogueContext> GetOptions(string database)
        {
            return new DbContextOptionsBuilder<BeerCatalogueContext>()
                .UseInMemoryDatabase(database)
                .Options;
        }
    }
}
