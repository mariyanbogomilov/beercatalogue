﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class FilterBeers_Should
    {
        [TestMethod]
        public void FilterStyle_Should_ReturnCorrectCount()
        {
            var options = BeerUtility.GetOptions(nameof(FilterStyle_Should_ReturnCorrectCount));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.Filter("style", "Svetla");
                var actual = filterCount.Result.Count();

                Assert.AreEqual(2, actual);
            }
        }
        [TestMethod]
        public void FilterCountry_Should_ReturnCorrectCount()
        {
            var options = BeerUtility.GetOptions(nameof(FilterCountry_Should_ReturnCorrectCount));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.Filter("country", "Bosnia");
                var actual = filterCount.Result.Count();

                Assert.AreEqual(2, actual);
            }
        }
    }
}
