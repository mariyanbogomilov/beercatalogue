﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class UpdateBeer_Should
    {
        [TestMethod]
        public void ReturnCorrect_Beer_When_UpdateIsSuccssesfull()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrect_Beer_When_UpdateIsSuccssesfull));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryId = brewery.Id,
                StyleId = style.Id
            };
            var beerDTO = new BeerDTO
            {
                Name = "ager",
                ABV = "4.4%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = brewery.Name,
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.UpdateBeer(beer.Id,beerDTO);

                var actual = result.Result;

                Assert.AreEqual(beerDTO.Name, actual.Name);
                Assert.AreEqual(beerDTO.ABV, actual.ABV);
            }
        }
        [TestMethod]
        public void GetCorrectMessage_When_BeerIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectMessage_When_BeerIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryId = brewery.Id,
                StyleId = style.Id
            };
            var beerDTO = new BeerDTO
            {
                Name = "ager",
                ABV = "4.4%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = brewery.Name,
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.UpdateBeer(33, beerDTO);


                var expected = "One or more errors occurred. (Value cannot be null.)";

                Assert.AreEqual(expected, result.Exception.Message);
            }
        }
    }
}
