﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class GetBeer_Should
    {
        [TestMethod]
        public void ReturnBeer_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnBeer_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 30,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var but = new BreweryService(date.Object, assertContext);

                var createBrewery = but.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 30
                });
                var result = sut.CreateBeer(beerDTO);

                var getBeer = sut.GetBeer(1);
                var res = getBeer.Result;

                Assert.AreEqual("Pale Lager", res.Name);
                Assert.AreEqual("4.8%", res.ABV);
                Assert.AreEqual("Zagorka Zagora", res.BreweryName);
                Assert.AreEqual("Svetla", res.StyleName);
            }
        }
    }
}
