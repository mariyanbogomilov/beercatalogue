﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class GetBeers_Should
    {
        [TestMethod]
        public void ReturnBeers_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnBeers_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 300,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 300
                });
                var result = sut.CreateBeer(beerDTO);

                var getBeer = sut.GetBeers();
                var actual = getBeer.Result.Count();

                Assert.AreEqual(1,actual);
            }
        }
    }
}
