﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class DeleteBeer_Should
    {
        [TestMethod]
        public void DeleteStyle_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(DeleteStyle_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer= new Beer
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryId = brewery.Id,
                StyleId = style.Id
            };
            var beerDTO = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = brewery.Name,
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.Delete(beer.Id);

                Assert.IsTrue(result.Result);
            }
        }
        [TestMethod]
        public void GetCorrectMessage_When_StyleIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectMessage_When_StyleIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.Delete(200);

                Assert.IsFalse(result.Result);
            }
        }
    }
}
