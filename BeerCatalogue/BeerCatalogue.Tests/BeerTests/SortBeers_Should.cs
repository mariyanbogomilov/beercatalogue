﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class SortBeers_Should
    {
        [TestMethod]
        public void ShouldSortBy_Name_Asc_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ShouldSortBy_Name_Asc_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.SortBy("name", "asc");
                var actualOne = filterCount.Result.First();
                var actualTwo = filterCount.Result.Last();

                Assert.AreEqual("Dark Lager", actualOne.Name);
                Assert.AreEqual("Pale Lager", actualTwo.Name);
            }
        }
        [TestMethod]
        public void ShouldSortBy_Name_Desc_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ShouldSortBy_Name_Desc_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.SortBy("name", "desc");
                var actualOne = filterCount.Result.First();
                var actualTwo = filterCount.Result.Last();

                Assert.AreEqual("Dark Lager", actualTwo.Name);
                Assert.AreEqual("Pale Lager", actualOne.Name);
            }
        }
        [TestMethod]
        public void ShouldSortBy_ABV_Asc_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ShouldSortBy_ABV_Asc_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.4%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.SortBy("abv", "desc");
                var actualOne = filterCount.Result.First();
                var actualTwo = filterCount.Result.Last();

                Assert.AreEqual("4.4%", actualTwo.ABV);
                Assert.AreEqual("4.8%", actualOne.ABV);
            }
        }
        [TestMethod]
        public void ShouldSortBy_ABV_Desc_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ShouldSortBy_ABV_Desc_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.4%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            var beerDTO2 = new BeerDTO
            {
                Name = "Dark Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });
                var createOneBeer = sut.CreateBeer(beerDTO1);
                var createTwoBeer = sut.CreateBeer(beerDTO2);

                var filterCount = sut.SortBy("abv", "asc");
                var actualOne = filterCount.Result.First();
                var actualTwo = filterCount.Result.Last();

                Assert.AreEqual("4.4%", actualOne.ABV);
                Assert.AreEqual("4.8%", actualTwo.ABV);
            }
        }
        [TestMethod]
        public void ShouldSortBy_Rating_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ShouldSortBy_Rating_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var user = new User
            {
                Id = 30,
                UserName = "goshko",
                Email = "goshko@gmai.com"
            };
            var country = new Country
            {
                Id = 33,
                Name = "Bosnia"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO1 = new BeerDTO
            {
                Id = 30,
                Name = "Pale Lager",
                ABV = "4.4%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = "Zagorka Zagora",
                StyleName = style.Name
            };

            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Countries.Add(country);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);
                var cut = new BreweryService(date.Object, assertContext);
                var rat = new BeerRatingService(date.Object, assertContext);

                var createBrewery = cut.Create(new BreweryDTO
                {
                    Name = "Zagorka Zagora",
                    CountryId = 33
                });

                var createOneBeer = sut.CreateBeer(beerDTO1);
                var addRating = rat.AddRating(new BeerRatingDTO
                {
                    BeerName = "Pale Lager",
                    Username = "goshko",
                    Rating = 3
                });
                var filterCount = sut.SortBy("rating", "asc");
                var actualOne = filterCount.Result.First();

                Assert.AreEqual(3, actualOne.Rating);
            }
        }
    }
}
