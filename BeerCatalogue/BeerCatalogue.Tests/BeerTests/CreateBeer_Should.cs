﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.BeerTests
{
    [TestClass]
    public class CreateBeer_Should
    {

        [TestMethod]
        public void ReturnCorrect_Beer_When_ParamsAreCorrect()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrect_Beer_When_ParamsAreCorrect));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beerDTO = new BeerDTO
            {
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = brewery.Name,
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.CreateBeer(beerDTO);

                var actual = result.Result;
                var b = assertContext.Beers.First();
                var get = sut.GetBeers();
                Assert.AreEqual(beerDTO.Name, actual.Name);
                Assert.AreEqual(beerDTO.Description, actual.Description);
                Assert.AreEqual(beerDTO.ABV, actual.ABV);
                Assert.AreEqual(beerDTO.Milliters, actual.Milliters);
                Assert.AreEqual(beerDTO.BreweryName, actual.BreweryName);
                Assert.AreEqual(beerDTO.StyleName, actual.StyleName);
            }
        }
        [TestMethod]
        public void ThrowException_When_ItemWith_Id_AlreadyExitsts()
        {
            var options = BeerUtility.GetOptions(nameof(ThrowException_When_ItemWith_Id_AlreadyExitsts));
            var date = new Mock<IDateTimeProvider>();
            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id =1,
                Name = "Zagorka",
                ABV = "4.8%",
                Description = "Bulgarka svetla bira",
                Milliters = 0.5f,
                BreweryId = brewery.Id,
                StyleId = style.Id
            };
            var beerDTO = new BeerDTO
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                BreweryName = brewery.Name,
                StyleName = style.Name
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.SaveChanges();
            }



            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerService(date.Object, assertContext);

                var result = sut.CreateBeer(beerDTO);

                var expectedMssg = "One or more errors occurred. (An item with the same key has already been added. Key: 1)";

                Assert.AreEqual(expectedMssg, result.Exception.Message);
            }
        }
    }
}
