﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace BeerCatalogue.Tests.StyleTests
{
    [TestClass]
    public class GetBeers_Should
    {
        [TestMethod]
        public void ReturnAllStyles_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnAllStyles_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var style = new Style
            {
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.SaveChanges();
            }
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.GetAllStyles();

                var actual = result.Result;

                Assert.AreEqual(1, actual.Count());
            }

        }
    }
}
