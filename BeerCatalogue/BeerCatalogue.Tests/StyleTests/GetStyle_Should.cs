﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.StyleTests
{
    [TestClass]
    public class GetBeer_Should
    {
        [TestMethod]
        public void ReturnStyle_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnStyle_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var style = new Style
            {
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.SaveChanges();
            }
            using (BeerCatalogueContext assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.GetStyle(style.Id);

                var actual = result.Result;

                Assert.AreEqual("Pale Lager", actual.Name);
                Assert.AreEqual("Pale lager is one of the most common beers", actual.Description);
            }
        }
    }
}
