﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.StyleTests
{
    [TestClass]
    public class CreateStyle_Should
    {
        [TestMethod]
        public void ReturnCorrect_Style_When_ParamsAreCorrect()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrect_Style_When_ParamsAreCorrect));
            var date = new Mock<IDateTimeProvider>();

            var styleDTO = new StyleDTO
            {
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var createStyle = new StyleService(date.Object,assertContext);

                var result = createStyle.CreateStyle(styleDTO);

                var actual = result.Result;

                Assert.AreEqual(styleDTO.Name, actual.Name);
                Assert.AreEqual(styleDTO.Description, actual.Description);
            }
        }
        [TestMethod]
        public void ThrowException_When_ItemWith_Id_AlreadyExitsts()
        {
            var options = BeerUtility.GetOptions(nameof(ThrowException_When_ItemWith_Id_AlreadyExitsts));
            var date = new Mock<IDateTimeProvider>();

            var style = new Style
            {
                Id =100,
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            var styleDTO = new StyleDTO
            {
                Id = 100,
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var createStyle = new StyleService(date.Object,assertContext);

                var result = createStyle.CreateStyle(styleDTO);

                var expectedMssg = "One or more errors occurred. (An item with the same key has already been added. Key: 100)";

                Assert.AreEqual(expectedMssg, result.Exception.Message);
            }
        }
    }
}
