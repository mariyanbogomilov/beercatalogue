﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.StyleTests
{
    [TestClass]
    public class DeleteBeer_Should
    {
        [TestMethod]
        public void DeleteStyle_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(DeleteStyle_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var style = new Style
            {
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };

            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.SaveChanges();
            }
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.Delete(style.Id);

                Assert.IsTrue(result.Result);
            }
        }
        [TestMethod]
        public void GetCorrectMessage_When_StyleIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectMessage_When_StyleIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.Delete(20);

                Assert.IsFalse(result.Result);
            }
        }
    }
}
