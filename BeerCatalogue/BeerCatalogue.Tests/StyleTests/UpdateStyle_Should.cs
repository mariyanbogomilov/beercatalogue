﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace BeerCatalogue.Tests.StyleTests
{
    [TestClass]
    public class UpdateBeer_Should
    {
        [TestMethod]
        public void UpdateStyle_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(UpdateStyle_Correctly));
            var date = new Mock<IDateTimeProvider>();
            var style = new Style
            {
                Name = "Pale Lager",
                Description = "Pale lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            var styleDTO = new StyleDTO
            {
                Name = "Dark Lager",
                Description = "Dark lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.SaveChanges();
            }
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.UpdateStyle(style.Id, styleDTO);

                var actual = result.Result;

                Assert.AreEqual("Dark Lager", actual.Name);
                Assert.AreEqual("Dark lager is one of the most common beers", actual.Description);
            }
        }
        [TestMethod]
        public void GetCorrectMessage_When_StyleIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(GetCorrectMessage_When_StyleIsNotFound));
            var date = new Mock<IDateTimeProvider>();


            var styleDTO = new StyleDTO
            {
                Name = "Dark Lager",
                Description = "Dark lager is one of the most common beers",
                CreatedOn = DateTime.UtcNow
            };
            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new StyleService(date.Object, assertContext);

                var result = sut.UpdateStyle(20, styleDTO);

                var expected = "One or more errors occurred. (Value cannot be null.)";

                Assert.AreEqual(expected, result.Exception.Message);
            }
        }
    }
}
