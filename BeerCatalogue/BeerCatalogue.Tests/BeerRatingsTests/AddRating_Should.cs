﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerCatalogue.Tests.BeerRatingsTests
{
    [TestClass]
    public class AddRating_Should
    {
        [TestMethod]
        public void Succsesfully_AddRating_ToBeer()
        {
            var options = BeerUtility.GetOptions(nameof(Succsesfully_AddRating_ToBeer));
            var date = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id=1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };
            var user = new User
            {
                Id = 1,
                UserName = "Georgi",
                Email = "georgi@gmail.com"                
            };

            var rating = new BeerRatingDTO
            {
                BeerName = beer.Name,
                Username = user.UserName,
                Rating = 3
            };
            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerRatingService(date.Object, assertContext);

                var result = sut.AddRating(rating);

                var actual = result.Result;
                var beerA = assertContext.Beers.First(x => x.Id == 1);
                var ratings = beerA.BeerRatings;
                var acutalRating = ratings.First().Rating;

                Assert.AreEqual(1, ratings.Count);
                Assert.AreEqual(3, acutalRating);
            }
        }

        [TestMethod]
        public void Throw_Exception_When_UserIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_Exception_When_UserIsNotFound));
            var date = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Name = "Zagorka Zagora"
            };
            var style = new Style
            {
                Name = "Svetla",
                Description = "Bulgarska svetla bira"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Pale Lager",
                ABV = "4.8%",
                Description = "Pale lager is one of the most common beers",
                Milliters = 0.5f,
                Brewery = brewery,
                Style = style
            };

            var rating = new BeerRatingDTO
            {
                BeerName = beer.Name,
                Username = "Gosho",
                Rating = 3
            };

            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Styles.Add(style);
                arrContext.Breweries.Add(brewery);
                arrContext.Beers.Add(beer);
                arrContext.SaveChanges();
            }

            using (var asserContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerRatingService(date.Object, asserContext);

                var rate = sut.AddRating(rating);

                var actual = rate.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null. (Parameter 'Beer or user does not exist!'))";

                Assert.AreEqual(expected, actual);
            }
        }
        [TestMethod]
        public void Throw_Exception_When_BeerIsNotFound()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_Exception_When_BeerIsNotFound));
            var date = new Mock<IDateTimeProvider>();
            var user = new User
            {
                Id = 1,
                UserName = "Georgi",
                Email = "georgi@gmail.com"
            };

            var rating = new BeerRatingDTO
            {
                BeerName = "Kola",
                Username = user.UserName,
                Rating = 3
            };

            using (var arrContext = new BeerCatalogueContext(options))
            {
                arrContext.Users.Add(user);
                arrContext.SaveChanges();
            }

            using (var asserContext = new BeerCatalogueContext(options))
            {
                var sut = new BeerRatingService(date.Object, asserContext);

                var rate = sut.AddRating(rating);

                var actual = rate.Exception.Message;
                var expected = "One or more errors occurred. (Value cannot be null. (Parameter 'Beer or user does not exist!'))";

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
