﻿using System;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerCatalogue.Tests.WishlistTests
{
    [TestClass]
    public class CreateWishlist_Should
    {
        [TestMethod]
        public void ReturnCorrectWishlist_When_ParamsAreValid()
        {
            var options = BeerUtility.GetOptions(nameof(ReturnCorrectWishlist_When_ParamsAreValid));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var wishlistDto = new WishlistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new WishlistService(assertContext);

                var result = sut.Create(wishlistDto);

                Assert.AreEqual(wishlistDto.UserId, result.Result.UserId);
                Assert.AreEqual(wishlistDto.BeerId, result.Result.BeerId);
            }
        }

        [TestMethod]
        public void Throw_When_WishlistBeerDoesNotExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_WishlistBeerDoesNotExists));

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var wishlistDto = new WishlistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Users.Add(user);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new WishlistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(wishlistDto));
            }
        }

        [TestMethod]
        public void Throw_When_WishlistUserDoesNotExists()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_WishlistUserDoesNotExists));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var wishlistDto = new WishlistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Beers.Add(beer);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new WishlistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(wishlistDto));
            }
        }

        [TestMethod]
        public void Throw_When_WishlistAlreadyExist()
        {
            var options = BeerUtility.GetOptions(nameof(Throw_When_WishlistAlreadyExist));

            var country = new Country
            {
                Id = 1,
                Name = "Czech"
            };

            var brewery = new Brewery
            {
                Id = 1,
                Name = "SomeBrewery",
                CountryId = 1
            };

            var style = new Style
            {
                Id = 1,
                Name = "TestStyle",
                Description = "Test Description"
            };

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer",
                BreweryId = 1,
                StyleId = 1
            };

            var wishlist = new Wishlist
            {
                BeerId = 1,
                UserId = 1
            };

            var wishlistDto = new WishlistDTO
            {
                BeerId = 1,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Styles.Add(style);
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.Wishlists.Add(wishlist);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new WishlistService(assertContext);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(wishlistDto));
            }
        }
    }
}
