﻿using System.Linq;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerCatalogue.Tests.WishlistTests
{
    [TestClass]
    public class GetAllBeersFromWishlist_Should
    {
        [TestMethod]
        public void GetAllBeersFromUserWishlist_Correctly()
        {
            var options = BeerUtility.GetOptions(nameof(GetAllBeersFromUserWishlist_Correctly));

            var user = new User
            {
                Id = 1,
                UserName = "UserOne",
                Email = "UserOne@yahoo.com"
            };

            var beer = new Beer
            {
                Id = 1,
                Name = "Staropramen",
                ABV = "5%",
                Description = "TestBeer1",
            };

            var secondBeer = new Beer
            {
                Id = 2,
                Name = "Kamenitza",
                ABV = "4.7%",
                Description = "TestBeer2",
            };

            var thirdBeer = new Beer
            {
                Id = 3,
                Name = "Ariana",
                ABV = "4.5%",
                Description = "TestBeer3",
            };

            var wishlist = new Wishlist()
            {
                BeerId = 1,
                UserId = 1
            };

            var secondWishlist = new Wishlist
            {
                BeerId = 3,
                UserId = 1
            };

            using (var arrangeContext = new BeerCatalogueContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.Beers.Add(secondBeer);
                arrangeContext.Beers.Add(thirdBeer);
                arrangeContext.Wishlists.Add(wishlist);
                arrangeContext.Wishlists.Add(secondWishlist);

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerCatalogueContext(options))
            {
                var sut = new WishlistService(assertContext);

                var result = sut.GetAllBeers(user.UserName);

                Assert.AreEqual(wishlist.BeerId, result.Result.ToArray()[0].Id);
                Assert.AreEqual(secondWishlist.BeerId, result.Result.ToArray()[1].Id);
            }
        }
    }
}
