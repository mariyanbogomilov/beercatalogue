﻿using System.ComponentModel.DataAnnotations;

namespace BeerCatalogueApp.Web.Models
{
    public class ReviewViewModel
    {
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 10, ErrorMessage = "Comment must be between 10 and 500 characters")]
        public string Comment { get; set; }
    }
}
