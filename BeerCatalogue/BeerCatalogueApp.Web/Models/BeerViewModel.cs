﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BeerCatalogueApp.Web.Models
{
    public class BeerViewModel
    {

        public int Id { get; set; }
        [Required]
        [StringLength(30,MinimumLength = 3,ErrorMessage = "Beer name should be between 3 and 30 characters")]
        public string Name { get; set; }
        public string Description { get; set; }

        public string ImageURL
        {
            get
            {
                return $"~/images/{this.Name}.jpg";
            }
        }
        [Required]
        public string ABV { get; set; }
        [Required]
        public float Milliters { get; set; }
        public int BreweryId { get; set; }
        public double Rating { get; set; }
        public string BreweryName { get; set; }
        public string StyleName { get; set; }
        public int StyleId { get; set; }
        public ICollection<ReviewViewModel> Reviews { get; set; } = new List<ReviewViewModel>();
        public ICollection<BeerRatingViewModel> BeerRatings { get; set; } = new List<BeerRatingViewModel>();
    }
}
