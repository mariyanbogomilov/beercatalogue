﻿namespace BeerCatalogueApp.Web.Models
{
    public class WishlistViewModel
    {
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
