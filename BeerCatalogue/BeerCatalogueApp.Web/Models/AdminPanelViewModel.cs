﻿using BeerCatalogue.Models;
using System.Collections.Generic;

namespace BeerCatalogueApp.Web.Models
{
    public class AdminPanelViewModel
    {
        public List<User> Users { get; set; } = new List<User>();
    }
}
