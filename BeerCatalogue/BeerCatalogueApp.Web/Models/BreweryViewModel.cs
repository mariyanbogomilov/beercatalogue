﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogueApp.Web.Models
{
    public class BreweryViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Brewery name is required")]
        [StringLength(40, MinimumLength = 5, ErrorMessage = "Brewery name should be between 5 and 40 symbols")]
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public ICollection<BeerViewModel> Beers { get; set; } = new List<BeerViewModel>();
    }
}
