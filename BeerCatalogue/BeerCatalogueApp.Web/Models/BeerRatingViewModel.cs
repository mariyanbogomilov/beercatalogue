﻿namespace BeerCatalogueApp.Web.Models
{
    public class BeerRatingViewModel
    {
        public int Rating { get; set; }
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
