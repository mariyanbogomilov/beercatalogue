﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogueApp.Web.Models
{
    public class StyleViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "You must have a name for style")]
        public string Name { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 10,ErrorMessage = "Description must be between 10 and 500 characters")]
        public string Description { get; set; }
        public ICollection<BeerViewModel> Beers { get; set; } = new List<BeerViewModel>();
    }
}
