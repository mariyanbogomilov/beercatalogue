﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BeerCatalogueApp.Web.Models
{
    public class CountryViewModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required(ErrorMessage = "Country name is required")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Country name should be between 3 and 60 symbols")]
        public string Name { get; set; }

        public string ImageURL
        {
            get
            {
                return $"~/images/{this.Name}.jpg";
            }
        }

        public ICollection<BreweryViewModel> Breweries { get; set; } = new List<BreweryViewModel>();
    }
}
