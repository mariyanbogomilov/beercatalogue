﻿using System.Threading.Tasks;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeerRatingsController : ControllerBase
    {
        private readonly IBeerRatingService beerRatingService;

        public BeerRatingsController(IBeerRatingService beerRatingService)
        {
            this.beerRatingService = beerRatingService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BeerRatingViewModel beerRatingViewModel)
        {
            if (beerRatingViewModel == null)
            {
                return BadRequest();
            }

            var beerRatingDTO = new BeerRatingDTO
            {
                Rating = beerRatingViewModel.Rating,
                BeerName = beerRatingViewModel.BeerName,
                Username = beerRatingViewModel.Username
            };

            var addRatingResult = await this.beerRatingService.AddRating(beerRatingDTO);

            return Created("post", addRatingResult);
        }
    }
}
