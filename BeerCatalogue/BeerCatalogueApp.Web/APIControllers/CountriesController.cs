﻿using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : Controller
    {

        private readonly ICountryService countryService;

        public CountriesController(ICountryService countryService)
        {
            this.countryService = countryService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll([FromQuery] int currentPage)
        {
            //var countries = await this.countryService.GetAllCountries(currentPage, 1);
            var countries = await this.countryService.GetAllCountries();
            return Ok(countries.Select(c=>c.GetModelAsObject()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var country =await this.countryService.GetCountry(id);

            if (country == null)
            {
                return NotFound();
            }

            return Ok(country.GetModelAsObject());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CountryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                Id = model.Id,
                Name = model.Name
            };

            var country =await this.countryService.Create(countryDTO);

            return Created("post", country);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CountryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                Id = model.Id,
                Name = model.Name
            };

            var updatedCountryDTO =await this.countryService.Update(id, countryDTO);

            return Ok(updatedCountryDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            var result =await this.countryService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
