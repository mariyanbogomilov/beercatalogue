﻿using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DranklistsController : ControllerBase
    {
        private readonly IDranklistService dranklistService;

        public DranklistsController(IDranklistService dranklistService)
        {
            this.dranklistService = dranklistService;
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] DranklistViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var dranklistDTO = new DranklistDTO
            {
                BeerName = model.BeerName,
                Username = model.Username
            };

            var createdDranklist = this.dranklistService.Create(dranklistDTO);

            return Created("post", createdDranklist);
        }
    }
}
