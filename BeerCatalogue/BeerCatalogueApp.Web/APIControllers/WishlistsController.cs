﻿using System.Threading.Tasks;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WishlistsController : ControllerBase
    {
        private readonly IWishlistService wishlistService;
        public WishlistsController(IWishlistService wishlistService)
        {
            this.wishlistService = wishlistService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] WishlistViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var wishlistDTO = new WishlistDTO
            {
                BeerName = model.BeerName,
                Username = model.Username
            };

            var createdWishlist = await this.wishlistService.Create(wishlistDTO);

            return Created("post", createdWishlist);
        }
    }
}
