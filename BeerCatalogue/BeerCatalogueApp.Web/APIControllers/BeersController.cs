﻿using System.Linq;
using System.Threading.Tasks;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeersController : Controller
    {

        private readonly IBeerService beerService;
        public BeersController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetBeers()
        {
            var beers = await this.beerService.GetBeers();

            return Ok(beers.Select(b => b.GetModelAsObject()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBeer(int id)
        {
            var beer =await this.beerService.GetBeer(id);

            if (beer == null)
            {
                return NotFound();
            }
            return Ok(beer.GetModelAsObject());
        }

        [HttpGet("filter")]
        public async Task<IActionResult> FilterBy([FromQuery] string type, [FromQuery] string name)
        {
            var beers =await this.beerService.Filter(type, name);

            if (beers == null)
            {
                return NotFound();
            }
            return Ok(beers.Select(b=>b.GetModelAsObject()));
        }

        [HttpGet("sortby")]
        public async Task<IActionResult> SortBy([FromQuery] string type, [FromQuery] string order)
        {
            var beers = await this.beerService.SortBy(type, order);

            if (beers == null)
            {
                return NotFound();
            }
            return Ok(beers.Select(b => b.GetModelAsObject()));
        }

        [HttpPost("")]
        public async Task<IActionResult> PostBeer([FromBody] BeerViewModel beer)
        {
            if (beer == null)
            {
                return BadRequest();
            }

            var beerDTO = new BeerDTO
            {
                Id = beer.Id,
                Name = beer.Name,
                ABV = beer.ABV,
                Description = beer.Description,
                Milliters = beer.Milliters,
                BreweryName = beer.BreweryName,
                StyleName = beer.StyleName
            };

            var br = await this.beerService.CreateBeer(beerDTO);

            return Created("post", br);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutBeer(int id, [FromBody] BeerViewModel beer)
        {
            if (beer == null)
            {
                return BadRequest();
            }

            var beerDTO = new BeerDTO
            {
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                Milliters = beer.Milliters,
            };

            var beerStyle =await this.beerService.UpdateBeer(id, beerDTO);

            return Ok(beerStyle);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBeer(int id)
        {
            var result =await this.beerService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
