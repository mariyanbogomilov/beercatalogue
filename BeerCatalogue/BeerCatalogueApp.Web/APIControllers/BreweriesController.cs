﻿using System.Linq;
using System.Threading.Tasks;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweriesController : ControllerBase
    {
        private readonly IBreweryService breweryService;

        public BreweriesController(IBreweryService breweryService)
        {
            this.breweryService = breweryService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var breweries =await this.breweryService.GetAllBreweries();

            return Ok(breweries.Select(br => br.GetModelAsObject()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var brewery =await this.breweryService.GetBrewery(id);

            if (brewery == null)
            {
                return NotFound();
            }


            return Ok(brewery.GetModelAsObject());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BreweryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO
            {
                Id = model.Id,
                Name = model.Name,
                CountryId = model.CountryId
            };

            var brewery =await this.breweryService.Create(breweryDTO);

            return Created("post", brewery);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BreweryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO
            {
                Id = model.Id,
                Name = model.Name,
                CountryName = model.CountryName
            };

            var brewery =await this.breweryService.Update(id, breweryDTO);

            return Ok(brewery);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrewery(int id)
        {
            var result =await this.breweryService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
