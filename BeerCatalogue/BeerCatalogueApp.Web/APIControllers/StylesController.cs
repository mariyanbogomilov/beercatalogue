﻿using System.Linq;
using System.Threading.Tasks;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StylesController : Controller
    {
        private readonly IStyleService styleService;

        public StylesController(IStyleService styleService)
        {
            this.styleService = styleService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var styles = await this.styleService.GetAllStyles();

            return Ok(styles.Select(s => s.GetModelAsObject()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var style =await this.styleService.GetStyle(id);
        
            if (style == null)
            {
                return NotFound();
            }
        
            return Ok(style.GetModelAsObject());
        }

        [HttpPost("")]
        public async Task<IActionResult> PostStyle([FromBody] StyleViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var styleDTo = new StyleDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };

            var style = await this.styleService.CreateStyle(styleDTo);

            return Created("post", style);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutStyle(int id, [FromBody] StyleViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var styleDTO = new StyleDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };

            var style = await this.styleService.UpdateStyle(id, styleDTO);

            return Ok(style);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStyle(int id)
        {
            var result = await this.styleService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}

