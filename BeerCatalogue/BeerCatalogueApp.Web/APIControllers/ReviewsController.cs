﻿using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerCatalogueApp.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            this.reviewService = reviewService;
        }


        [HttpGet("")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await this.reviewService.GetAllReviews(id));
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] ReviewViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var reviewDTO = new ReviewDTO
            {
                BeerName = model.BeerName,
                Username = model.Username,
                Comment = model.Comment       
            };

            var createdReview = await this.reviewService.Create(reviewDTO);

            return Created("post", createdReview);
        }

    }
}
