﻿using BeerCatalogue.Database;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace BeerCatalogueApp.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly BeerCatalogueContext _context;
        private readonly IBreweryService breweryService;
        private readonly IVMMapper<BreweryDTO, BreweryViewModel> mapper;
        private readonly IBeerService beerService;
        private readonly IVMMapper<BeerDTO, BeerViewModel> beerMapper;

        public BreweriesController(BeerCatalogueContext context, IBreweryService breweryService,
            IVMMapper<BreweryDTO, BreweryViewModel> mapper, IBeerService beerService,
            IVMMapper<BeerDTO, BeerViewModel> beerMapper)
        {
            this.breweryService = breweryService;
            this.mapper = mapper;
            this.beerService = beerService;
            this.beerMapper = beerMapper;
            _context = context;
        }

        public async Task<IActionResult> GetBreweriesForCountry(int id)
        {
            var breweries = await this.breweryService.GetBreweriesForCountry(id);

            var brModels = breweries.Select(breweries => this.mapper.MapFromDTOToVM(breweries));

            return View(brModels);
        }
        // GET: Breweries
        public async Task<IActionResult> Index(int page = 1, string sortExpression = "Name")
        {
            var breweries = await this.breweryService.GetAllBreweries();

            var breweriesViewModel = breweries.Select(brewery => this.mapper.MapFromDTOToVM(brewery));

            var breweriesPaging = PagingList.Create(breweriesViewModel, 4, page, sortExpression, "Name");

            return View(breweriesPaging);
        }

        // GET: Breweries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            BreweryDTO brewery;

            try
            {
                brewery = await this.breweryService.GetBrewery(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            var breweryViewModel = this.mapper.MapFromDTOToVM(brewery);

            return View(breweryViewModel);
        }

        // GET: Breweries/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["CountryId"] = new SelectList(_context.Countries, "Id", "Name");

            return View();
        }

        // POST: Breweries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,CountryId")] BreweryViewModel brewery)
        {
            if (ModelState.IsValid)
            {
                var breweryDTO = this.mapper.MapFromVMToDTO(brewery);

                await this.breweryService.Create(breweryDTO);

                return RedirectToAction(nameof(Index));
            }
            ViewData["CountryId"] = new SelectList(_context.Countries, "Id", "Name", brewery.CountryId);
            return View(brewery);
        }

        // GET: Breweries/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            BreweryDTO brewery;

            try
            {
                brewery = await this.breweryService.GetBrewery(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            var breweryVM = this.mapper.MapFromDTOToVM(brewery);
            //TODO: WHERE IS DELETED !
            ViewData["CountryId"] = new SelectList(_context.Countries.Where(c => c.IsDeleted == false), "Id", "Name", breweryVM.CountryId);
            return View(breweryVM);
        }

        // POST: Breweries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,CountryId")] BreweryViewModel brewery)
        {
            if (id != brewery.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var breweryToDTO = this.mapper.MapFromVMToDTO(brewery);

                    await this.breweryService.Update(id, breweryToDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BreweryExists(brewery.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CountryId"] = new SelectList(_context.Countries, "Id", "Name", brewery.CountryId);
            return View(brewery);
        }

        // GET: Breweries/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            BreweryDTO brewery; 

            try
            {
                brewery = await this.breweryService.GetBrewery(id);
            }
            catch (System.Exception)
            {
                return NotFound(); ;
            }

            var breweryViewModel = this.mapper.MapFromDTOToVM(brewery);

            return View(breweryViewModel);
        }
        //TODO: CHECK FOR DELETED ONES
        // POST: Breweries/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.breweryService.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Beers(int id)
        {
            var beers = await this.beerService.BeersByBrewery(id);

            var beersViewModel = beers.Select(b => this.beerMapper.MapFromDTOToVM(b));

            return View(beersViewModel);
        }

        private bool BreweryExists(int id)
        {
            return _context.Breweries.Any(e => e.Id == id);
        }
    }
}
