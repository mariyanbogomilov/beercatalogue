﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using Microsoft.AspNetCore.Authorization;
using BeerCatalogue.Services.DTOsEntities;
using Microsoft.AspNetCore.Identity;
using BeerCatalogue.Services.Contracts;
using BeerCatalogueApp.Web.VMMapper;

namespace BeerCatalogueApp.Web.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly IReviewService reviewService;
        private readonly UserManager<User> userManager;
        private readonly BeerCatalogueContext _context;

        public ReviewsController(UserManager<User> userManager, IReviewService reviewService, BeerCatalogueContext context)
        {
            this.reviewService = reviewService;
            this.userManager = userManager;
            this._context = context;
        }

        // GET: Reviews
        public async Task<IActionResult> Index(int id)
        {
            var reviewsDTO = await reviewService.GetAllReviews(id);
            var reviews = reviewsDTO.Select(x => x.GetViem());
            return View(reviews);
        }

        [Authorize]
        public async Task<IActionResult> UserReviews()
        {
            var user = await this.userManager.GetUserAsync(this.User);

            var reviewDTO = await reviewService.GetReviewsForUser(user.Id);
            var reviews = reviewDTO.Select(x => x.GetViem());
            return View(reviews);
        }
        [Authorize]
        // GET: Reviews/Create
        public IActionResult Create(int id)
        {
            TempData["BeerName"] = this._context.Beers.FirstOrDefaultAsync(b => b.Id == id).Result.Name;
            return View();
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Comment,BeerId,UserId,BeerName,Username")] ReviewDTO review)
        {

            var name = TempData["BeerName"].ToString();
            var user = await this.userManager.GetUserAsync(this.User);
            review.Username = user.UserName;
            review.BeerName = name;
            try
            {
                if (ModelState.IsValid)
                {
                    await reviewService.Create(review);
                    return RedirectToAction("Index", "Beers");
                }
            }
            catch
            {
                ViewBag.Message = "You already made a review for this beer,check your reviews for more info";
                return View();
            }
            return View(review);
        }

        [Authorize]
        public async Task<IActionResult> Remove(int beerId,int userId)
        {
            var result = await this.reviewService.Delete(beerId, userId);

            if (result == false)
            {
                return BadRequest();
            }

            return RedirectToAction("Index","Beers");

        }
    }
}
