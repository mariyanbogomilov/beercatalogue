﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerCatalogue.Database;
using BeerCatalogue.Services.Contracts;
using BeerCatalogueApp.Web.Models;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.VMMapper;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using Microsoft.AspNetCore.Routing;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Authorization;

namespace BeerCatalogueApp.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService beerService;
        private readonly BeerCatalogueContext _context;
        private readonly IVMMapper<BeerDTO, BeerViewModel> beerMapper;

        public BeersController(IBeerService beerService, BeerCatalogueContext _context, IVMMapper<BeerDTO, BeerViewModel> beerMapper)
        {
            this.beerService = beerService;
            this._context = _context;
            this.beerMapper = beerMapper;
        }

        public async Task<IActionResult> Index(string filter, int page = 1, string sortExpression = "Name")
        {
            var beerContext = await this.beerService.GetBeers();

            if (!String.IsNullOrWhiteSpace(filter))
            {
                beerContext = beerContext.Where(b => b.Name.ToLower().Contains(filter));
            }
            
            var beersViewModel = beerContext.Select(b => this.beerMapper.MapFromDTOToVM(b));
            
            var beers = PagingList.Create(beersViewModel, 4, page, sortExpression, "Name");

            beers.RouteValue = new RouteValueDictionary
            {
                {"filter", filter }
            };

            return View(beers);
        }
        public async Task<IActionResult> Details(int id)
        {
            BeerDTO beer;

            try
            {
                beer = await beerService.GetBeer(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
            
            var beerMod = beer.GetView();
            return View(beerMod);
        }

        // GET: Beers/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["BreweryName"] = new SelectList(_context.Breweries, "Name", "Name");
            ViewData["StyleName"] = new SelectList(_context.Styles, "Name", "Name");
            return View();
        }

        // POST: Beers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,ABV,Milliters,BreweryId,BreweryName,StyleId,StyleName,CreatedOn,ModifiedOn,DeletedOn,IsDeleted")] BeerDTO beer)
        {
            if (ModelState.IsValid)
            {
                await this.beerService.CreateBeer(beer);

                return RedirectToAction(nameof(Index));
            }
            return View(beer);
        }

        // GET: Beers/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            BeerDTO beer;

            try
            {
                beer = await beerService.GetBeer(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            var beerModel = beer.GetView();
            return View(beerModel);
        }

        // POST: Beers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,ABV,Milliters,BreweryId,StyleId,CreatedOn,ModifiedOn,DeletedOn,IsDeleted")] BeerDTO beer)
        {
            if (ModelState.IsValid)
            {
                await this.beerService.UpdateBeer(id, beer);
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        // GET: Beers/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            BeerDTO beer;

            try
            {
                beer = await beerService.GetBeer(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            var beerMod = beer.GetView();
            return View(beerMod);
        }

        // POST: Beers/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.beerService.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> FilterByCountry(int id)
        {
            var countryName = this._context.Countries.FirstOrDefaultAsync(s => s.Id == id).Result.Name;
            //TODO: not complete at all
            var beers = await this.beerService.Filter("country", countryName);

            var br = beers.Select(x => x.GetView());
            return View(br);
        }
        public async Task<IActionResult> FilterByStyle(int id)
        {
            var styleName = this._context.Styles.FirstOrDefaultAsync(s => s.Id == id).Result.Name;
            //TODO: not complete at all
            var beers = await this.beerService.Filter("style", styleName);

            var br = beers.Select(x => x.GetView());
            return View(br);
        }
    }
}
