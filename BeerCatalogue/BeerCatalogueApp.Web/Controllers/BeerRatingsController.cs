﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace BeerCatalogueApp.Web.Controllers
{
    [Authorize]
    public class BeerRatingsController : Controller
    {
        private readonly BeerCatalogueContext _context;
        private readonly UserManager<User> userManager;
        private readonly IBeerRatingService beerRatingService;

        public BeerRatingsController(BeerCatalogueContext context, IBeerRatingService beerRatingService
            , UserManager<User> userManager)
        {
            _context = context;
            this.userManager = userManager;
            this.beerRatingService = beerRatingService;
        }

        // GET: BeerRatings/Create
        [Authorize]
        public IActionResult Create(int id)
        {
            TempData["BeerName"] = this._context.Beers.FirstOrDefaultAsync(b => b.Id == id).Result.Name;
            return View();
        }

        // POST: BeerRatings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Rating,BeerName,BeerId,UserId,Username")] BeerRatingDTO beerRating)
        {
            var name = TempData["BeerName"].ToString();
            var user = await this.userManager.GetUserAsync(this.User);
            beerRating.Username = user.UserName;
            beerRating.BeerName = name;
            try
            {
                if (ModelState.IsValid)
                {
                    await beerRatingService.AddRating(beerRating);
                    return RedirectToAction("Index", "Beers");
                }
            }
            catch
            {
                ViewBag.Message = "You have already rated this beer go back and choose another";
                return View();
            }

            return View(beerRating);
        }
    }
}
