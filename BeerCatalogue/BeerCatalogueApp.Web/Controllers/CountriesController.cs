﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BeerCatalogue.Database;
using BeerCatalogue.Services.Contracts;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Authorization;
using ReflectionIT.Mvc.Paging;

namespace BeerCatalogueApp.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly BeerCatalogueContext _context;
        private readonly ICountryService countryService;
        private readonly IVMMapper<CountryDTO, CountryViewModel> mapper;

        public CountriesController(BeerCatalogueContext context, ICountryService countryService,
            IVMMapper<CountryDTO, CountryViewModel> mapper)
        {
            _context = context;
            this.countryService = countryService;
            this.mapper = mapper;
        }

        // GET: Countries
        public async Task<IActionResult> Index(int page = 1, string sortExpression = "Name")
        {
            var countries = await this.countryService.GetAllCountries();

            var countriesViewModels = countries.Select(countries => this.mapper.MapFromDTOToVM(countries));

            var countriesPaging = PagingList.Create(countriesViewModels, 4, page, sortExpression, "Name");

            return View(countriesPaging);
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            CountryDTO country;

            try
            {
                country = await this.countryService.GetCountry(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            var countryViewModel = this.mapper.MapFromDTOToVM(country);

            return View(countryViewModel);
        }

        // GET: Countries/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] CountryViewModel country)
        {
            if (ModelState.IsValid)
            {
                var countryDTO = this.mapper.MapFromVMToDTO(country);

                await this.countryService.Create(countryDTO);

                return RedirectToAction(nameof(Index));
            }

            return View(country);
        }

        // GET: Countries/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            CountryDTO country;

            try
            {
                country = await this.countryService.GetCountry(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            var countryViewModel = this.mapper.MapFromDTOToVM(country);

            return View(countryViewModel);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] CountryViewModel country)
        {
            if (id != country.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var countryDTO = this.mapper.MapFromVMToDTO(country);

                    await this.countryService.Update(id, countryDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CountryExists(country.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(country);
        }

        // GET: Countries/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            CountryDTO country;

            try
            {
                country = await this.countryService.GetCountry(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            var countryViewModel = this.mapper.MapFromDTOToVM(country);

            return View(countryViewModel);
        }
        //TODO: CHECK FOR DELETED COUNTRIES
        // POST: Countries/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.countryService.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }
    }
}
