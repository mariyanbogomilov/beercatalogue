﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogueApp.Web.Controllers
{
    [Authorize]
    public class WishlistsController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IWishlistService wishlistService;
        private readonly IVMMapper<BeerDTO, BeerViewModel> beerMapper;
        private readonly IBeerService beerService;

        public WishlistsController(UserManager<User> userManager, IWishlistService wishlistService, IVMMapper<BeerDTO, BeerViewModel> beerMapper,
             IBeerService beerService)
        {
            this.userManager = userManager;
            this.wishlistService = wishlistService;
            this.beerMapper = beerMapper;
            this.beerService = beerService;
        }

        public async Task<IActionResult> Index()
        {
            if (TempData["alreadyExist"] != null)
            {
                ViewBag.Message = TempData["alreadyExist"].ToString();
            }

            var user = await this.userManager.GetUserAsync(this.User);

            var userBeersInWishlist = await this.wishlistService.GetAllBeers(user.UserName);

            var userBeersInWishlistViewModel = userBeersInWishlist.Select(b => this.beerMapper.MapFromDTOToVM(b));

            return View(userBeersInWishlistViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Remove(int id)
        {
            var user = await this.userManager.GetUserAsync(this.User);

            var result = await this.wishlistService.Remove(id, user.UserName);

            if (result == false)
            {
                return BadRequest();
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Add(int id)
        {
            try
            {
                int user = int.Parse(this.userManager.GetUserId(this.User));

                var beer = await this.beerService.GetBeer(id);

                var wishlist = new WishlistDTO
                {
                    UserId = user,
                    BeerId = beer.Id
                };

                await this.wishlistService.Create(wishlist);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                TempData["alreadyExist"] = "Beer is already in your wishlist!";
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
