﻿using System.Linq;
using System.Threading.Tasks;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BeerCatalogueApp.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUserService userService;
        private readonly BeerCatalogueContext _context;
        private readonly UserManager<User> userManager;
        public AdminController(IUserService userService, BeerCatalogueContext context, UserManager<User> userManager)
        {
            this.userService = userService;
            this._context = context;
            this.userManager = userManager;
        }

        [HttpGet]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> Users()
        {
            var vm = new AdminPanelViewModel();
            vm.Users = (await this.userService.GetAllUsers()).ToList();
            return View(vm);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> BanUser(int id)
        {
            var user = await this._context.Users.FirstOrDefaultAsync(u => u.Id == id);
            await this.userManager.RemoveFromRoleAsync(user, "User");
            await this.userManager.AddToRoleAsync(user, "Banned");

            return RedirectToAction("Index","Home");
        }
    }
}
