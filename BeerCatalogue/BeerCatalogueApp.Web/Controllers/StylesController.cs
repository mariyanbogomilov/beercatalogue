﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.VMMapper;
using Microsoft.AspNetCore.Authorization;
using ReflectionIT.Mvc.Paging;

namespace BeerCatalogueApp.Web.Controllers
{
    public class StylesController : Controller
    {
        private readonly IStyleService styleService;

        public StylesController(IStyleService styleService)
        {
            this.styleService = styleService;
        }

        // GET: Styles
        public async Task<IActionResult> Index(int page, string sortExpression = "Name")
        {
            var styles = await styleService.GetAllStyles();

            var result = styles.Select(x => x.GetViem());

            var stylesPaging = PagingList.Create(result, 4, page, sortExpression, "Name");

            return View(stylesPaging);
        }

        // GET: Styles/Details/5
        public async Task<IActionResult> Details(int id)
        {
            StyleDTO style;

            try
            {
                style = await styleService.GetStyle(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            var styleModel = style.GetViem();
            return View(styleModel);
        }

        // GET: Styles/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,CreatedOn,ModifiedOn,DeletedOn,IsDeleted")] StyleDTO style)
        {
            if (ModelState.IsValid)
            {
                var styleDTO = await styleService.CreateStyle(style);

                return RedirectToAction(nameof(Index));
            }
            return View(style);
        }
        // GET: Styles/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            StyleDTO style;

            try
            {
                style = await styleService.GetStyle(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            var styleModel = style.GetViem();
            return View(styleModel);
        }
        // POST: Styles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,CreatedOn,ModifiedOn,DeletedOn,IsDeleted")] StyleDTO style)
        {
            if (id != style.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await styleService.UpdateStyle(id, style);
                return RedirectToAction(nameof(Index));
            }
            return View(style);
        }

        // GET: Styles/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            StyleDTO style;

            try
            {
                style = await styleService.GetStyle(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            var styleView = style.GetViem();
            return View(styleView);
        }

        // POST: Styles/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.styleService.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        //private bool StyleExists(int id)
        //{
        //    return _context.Styles.Any(e => e.Id == id);
        //}
    }
}
