﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BeerCatalogueApp.Web.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.Services;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using System.Threading.Tasks;
using System.Linq;

namespace BeerCatalogueApp.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerService beerService;
        private readonly IVMMapper<BeerDTO, BeerViewModel> beerMapper;

        public HomeController(ILogger<HomeController> logger, IBeerService beerService,
            IVMMapper<BeerDTO, BeerViewModel> beerMapper)
        {
            this.beerMapper = beerMapper;
            this.beerService = beerService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var beers = await this.beerService.TopRatedBeers();

            var beersViewModel = beers.Select(b => this.beerMapper.MapFromDTOToVM(b));

            return View(beersViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
