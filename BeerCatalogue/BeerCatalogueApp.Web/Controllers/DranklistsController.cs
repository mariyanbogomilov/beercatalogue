﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogueApp.Web.Controllers
{
    [Authorize]
    public class DranklistsController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IDranklistService dranklistService;
        private readonly IVMMapper<BeerDTO, BeerViewModel> beerMapper;
        private readonly IBeerService beerService;

        public DranklistsController(UserManager<User> userManager, IDranklistService dranklistService, IVMMapper<BeerDTO, BeerViewModel> beerMapper,
             IBeerService beerService)
        {
            this.userManager = userManager;
            this.dranklistService = dranklistService;
            this.beerMapper = beerMapper;
            this.beerService = beerService;
        }

        public async Task<IActionResult> Index()
        {
            if (TempData["alreadyExist"] != null)
            {
                ViewBag.Message = TempData["alreadyExist"].ToString();
            }

            var user = await this.userManager.GetUserAsync(this.User);

            var userBeersInDranklist = await this.dranklistService.GetAllBeers(user.UserName);

            var userBeersInDranklistViewModel = userBeersInDranklist.Select(b => this.beerMapper.MapFromDTOToVM(b));

            return View(userBeersInDranklistViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Remove(int id)
        {
            var user = await this.userManager.GetUserAsync(this.User);

            var result = await this.dranklistService.Remove(id, user.UserName);

            if (result == false)
            {
                return BadRequest();
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Add(int id)
        {
            try
            {
                var user = await this.userManager.GetUserAsync(this.User);

                var beer = await this.beerService.GetBeer(id);


                var dranklist = new DranklistDTO
                {
                    UserId = user.Id,
                    BeerId = beer.Id
                };

                await this.dranklistService.Create(dranklist);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                TempData["alreadyExist"] = "Beer is already in your dranklist!";
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
