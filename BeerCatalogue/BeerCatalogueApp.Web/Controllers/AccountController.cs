﻿using System.Threading.Tasks;
using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogueApp.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BeerCatalogueApp.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly BeerCatalogueContext context;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, BeerCatalogueContext context)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.context = context;
        }

        [HttpGet]
        public IActionResult Register()
        {
            var vm = new RegisterViewModel();
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Email, Email = model.Email };
                var result = await this.userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    if (user.UserName.Contains("admin123"))
                    {
                        await this.userManager.AddToRoleAsync(user, "Admin");
                    }
                    else
                    {
                        await this.userManager.AddToRoleAsync(user, "User");
                    }
                    await this.signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Error", "Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            //var vm = new LoginViewModel();
            //return View(vm);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);

            if (user != null)
            {
                if (await this.userManager.IsInRoleAsync(user, "Banned"))
                {
                    ViewBag.Message = "Account has been banned";
                    return View();
                }
                if (ModelState.IsValid)
                {
                    var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: true, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }

            }

            ViewBag.Message = "Account does not exist";
            return View();
        }

    }
}
