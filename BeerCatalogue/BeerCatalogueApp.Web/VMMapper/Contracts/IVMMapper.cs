﻿namespace BeerCatalogueApp.Web.VMMapper.Contracts
{
    public interface IVMMapper<TDTO, TVM>
    {
        TVM MapFromDTOToVM(TDTO dto);

        TDTO MapFromVMToDTO(TVM tvm);
    }
}
