﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;

namespace BeerCatalogueApp.Web.VMMapper
{
    public static class BeerViewMapper
    {
        internal static BeerViewModel GetView(this BeerDTO item)
       => item == null ? null : new BeerViewModel
       {
           Id = item.Id,
           Name = item.Name,
           Description = item.Description,
           ABV = item.ABV,
           Milliters = item.Milliters,
           Rating = item.Rating,
           BreweryId = item.BreweryId,
           BreweryName = item.BreweryName,
           StyleId = item.StyleId,
           StyleName = item.StyleName
            //TODO: da se dopishat kolekciq review
        };

        //ne ni trqbva getdto
        internal static BeerDTO GetDTO(this BeerViewModel item)
            => item == null ? null : new BeerDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                ABV = item.ABV,
                Milliters = item.Milliters,
                BreweryId = item.BreweryId,
                BreweryName = item.BreweryName,
                StyleId = item.StyleId,
                StyleName = item.StyleName
                //TODO:  da se dopishat kolekciq review
            };
    }
}
