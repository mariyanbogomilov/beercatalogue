﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;

namespace BeerCatalogueApp.Web.VMMapper
{
    public class UserMapper : IVMMapper<UserDTO, UserViewModel>
    {
        public UserViewModel MapFromDTOToVM(UserDTO userDTO)
        {
            return new UserViewModel
            {
                Id = userDTO.Id,
                Username = userDTO.Username,
                Password = userDTO.Password,
                Email = userDTO.Email,
                Age = userDTO.Age
            };
        }

        public UserDTO MapFromVMToDTO(UserViewModel userVM)
        {
            return new UserDTO
            {
                Id = userVM.Id,
                Username = userVM.Username,
                Password = userVM.Password,
                Email = userVM.Email,
                Age = userVM.Age
            };
        }
    }
}
