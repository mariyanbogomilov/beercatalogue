﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using System.Linq;

namespace BeerCatalogueApp.Web.VMMapper
{
    public static class StyleMapper
    {
        internal static StyleViewModel GetViem(this StyleDTO style)
        => style == null ? null : new StyleViewModel
        {
            Id = style.Id,
            Name = style.Name,
            Description = style.Description,
            Beers = style.Beers.Select(b=> b.GetView()).ToList()
        };

        internal static StyleDTO GetDTO(this StyleViewModel style)
        => style == null ? null : new StyleDTO
        {
            Id = style.Id,
            Name = style.Name,
            Description = style.Description,
            Beers = style.Beers.Select(b=>b.GetDTO()).ToList()
        };
    }
}
