﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using System.Linq;

namespace BeerCatalogueApp.Web.VMMapper
{
    public class BreweryMapper : IVMMapper<BreweryDTO, BreweryViewModel>
    {
        private readonly IVMMapper<BeerDTO, BeerViewModel> mapper;

        public BreweryMapper(IVMMapper<BeerDTO, BeerViewModel> mapper)
        {
            this.mapper = mapper;
        }

        public BreweryViewModel MapFromDTOToVM(BreweryDTO breweryDTO)
        {
            return new BreweryViewModel
            {
                Id = breweryDTO.Id,
                Name = breweryDTO.Name,
                CountryId = breweryDTO.CountryId,
                CountryName = breweryDTO.CountryName,
                Beers = breweryDTO.Beers
                .Select(b => this.mapper.MapFromDTOToVM(b))
                .ToList()
            };
        }

        public BreweryDTO MapFromVMToDTO(BreweryViewModel breweryVM)
        {
            return new BreweryDTO
            {
                Id = breweryVM.Id,
                Name = breweryVM.Name,
                CountryId = breweryVM.CountryId,
                CountryName = breweryVM.CountryName,
                Beers = breweryVM.Beers
                .Select(b => this.mapper.MapFromVMToDTO(b))
                .ToList()
            };
        }
    }
}
