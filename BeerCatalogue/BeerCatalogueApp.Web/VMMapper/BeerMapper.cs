﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;

namespace BeerCatalogueApp.Web.VMMapper
{
    public class BeerMapper : IVMMapper<BeerDTO, BeerViewModel>
    {
        public BeerViewModel MapFromDTOToVM(BeerDTO beerDTO)
        {
            return new BeerViewModel
            {
                Id = beerDTO.Id,
                Name = beerDTO.Name,
                Description = beerDTO.Description,
                ABV = beerDTO.ABV,
                Milliters = beerDTO.Milliters,
                Rating = beerDTO.Rating,
                BreweryId = beerDTO.BreweryId,
                BreweryName = beerDTO.BreweryName,
                StyleId = beerDTO.StyleId,
                StyleName = beerDTO.StyleName
            };
        }

        public BeerDTO MapFromVMToDTO(BeerViewModel beerVM)
        {
            return new BeerDTO
            {
                Id = beerVM.Id,
                Name = beerVM.Name,
                Description = beerVM.Description,
                ABV = beerVM.ABV,
                Milliters = beerVM.Milliters,
                BreweryId = beerVM.BreweryId,
                BreweryName = beerVM.BreweryName,
                StyleId = beerVM.StyleId,
                StyleName = beerVM.StyleName
            };
        }
    }
}
