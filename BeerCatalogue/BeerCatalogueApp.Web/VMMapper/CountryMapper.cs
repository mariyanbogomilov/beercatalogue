﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using System.Linq;

namespace BeerCatalogueApp.Web.VMMapper
{
    public class CountryMapper : IVMMapper<CountryDTO, CountryViewModel>
    {
        private readonly IVMMapper<BreweryDTO, BreweryViewModel> mapper;

        public CountryMapper(IVMMapper<BreweryDTO, BreweryViewModel> mapper)
        {
            this.mapper = mapper;
        }

        public CountryViewModel MapFromDTOToVM(CountryDTO countryDTO)
        {
            return new CountryViewModel
            {
                Id = countryDTO.Id,
                Name = countryDTO.Name,
                Breweries = countryDTO.Breweries.Select(br => this.mapper.MapFromDTOToVM(br)).ToList()
            };
        }

        public CountryDTO MapFromVMToDTO(CountryViewModel countryViewModel)
        {
            return new CountryDTO
            {
                Name = countryViewModel.Name
            };
        }
    }
}
