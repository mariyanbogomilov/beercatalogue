﻿using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogueApp.Web.Models;

namespace BeerCatalogueApp.Web.VMMapper
{
    public static class ReviewViewMapper
    {
        internal static ReviewViewModel GetViem(this ReviewDTO review)
        => review == null ? null : new ReviewViewModel
        {
            Comment = review.Comment,
            BeerId = review.BeerId,
            BeerName = review.BeerName,
            UserId = review.UserId,
            Username = review.Username
        };

        internal static ReviewDTO GetDTO(this ReviewViewModel review)
        => review == null ? null : new ReviewDTO
        {
            Comment = review.Comment,
            BeerId = review.BeerId,
            BeerName = review.BeerName,
            UserId = review.UserId,
            Username = review.Username
        };
    }
}
