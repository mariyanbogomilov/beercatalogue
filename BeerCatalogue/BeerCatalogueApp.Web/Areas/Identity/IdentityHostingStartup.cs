﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(BeerCatalogueApp.Web.Areas.Identity.IdentityHostingStartup))]
namespace BeerCatalogueApp.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}