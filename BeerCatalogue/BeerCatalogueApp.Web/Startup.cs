using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using BeerCatalogue.Services.DTOsProviders;
using BeerCatalogue.Services.Services;
using BeerCatalogueApp.Web.Models;
using BeerCatalogueApp.Web.VMMapper;
using BeerCatalogueApp.Web.VMMapper.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ReflectionIT.Mvc.Paging;

namespace BeerCatalogueApp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BeerCatalogueContext>
               (x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IEmailSender, EmailSender>();

            services
               .AddIdentity<User, Role>(config =>
               {
                   config.SignIn.RequireConfirmedAccount = false;
                   config.Password.RequiredLength = 6;
                   config.Password.RequireLowercase = false;
                   config.Password.RequireUppercase = false;
                   config.Password.RequireNonAlphanumeric = false;
               })
               .AddEntityFrameworkStores<BeerCatalogueContext>()
               .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "Identity.Cookie";
                options.LoginPath = "/Account/Login";
            });

            services.AddRazorPages();
            services.AddControllersWithViews();

            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IBeerService, BeerService>();
            services.AddScoped<IBreweryService, BreweryService>();
            services.AddScoped<IBeerRatingService, BeerRatingService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IWishlistService, WishlistService>();
            services.AddScoped<IDranklistService, DranklistService>();
            services.AddScoped<IStyleService, StyleService>();
            services.AddScoped<IVMMapper<BeerDTO, BeerViewModel>, BeerMapper>();
            services.AddScoped<IVMMapper<BreweryDTO, BreweryViewModel>, BreweryMapper>();
            services.AddScoped<IVMMapper<CountryDTO, CountryViewModel>, CountryMapper>();
            services.AddScoped<IVMMapper<UserDTO, UserViewModel>, UserMapper>();
            services.AddPaging(options =>
            {
                options.ViewName = "Bootstrap4";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/Error/{0}");
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });
        }
    }
}
