﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using System.Linq;

namespace BeerCatalogue.Services.DTOsMapper
{
    public static class StyleDTOMapper
    {
        internal static StyleDTO GetDTO(this Style item)
            => item == null ? null : new StyleDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                CreatedOn = item.CreatedOn,
                IsDeleted = item.IsDeleted,
                Beers = item.Beers.Select(b=>b.GetDTO()).ToList()
            };

        internal static Style GetModel(this StyleDTO item)
            => item == null ? null : new Style
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                CreatedOn = item.CreatedOn,
                IsDeleted = item.IsDeleted,
                Beers = item.Beers.Select(b => b.GetModel()).ToList()
            };

        public static object GetModelAsObject(this StyleDTO item)
           => item == null ? null :
               new
               {
                   Name = item.Name,
                   Description = item.Description,
                   Beers = item.Beers.Select(r => new
                   {
                       Name = r.Name,
                       ABV = r.ABV
                   })
               };
    }
}
