﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using System.Linq;

namespace BeerCatalogue.Services.DTOsMapper
{
    public static class BeerDTOMapper
    {
        internal static BeerDTO GetDTO(this Beer item)
        => item == null ? null : new BeerDTO
        {
            Id = item.Id,
            Name = item.Name,
            Description = item.Description,
            ABV = item.ABV,
            Milliters = item.Milliters,
            BreweryId = item.BreweryId,
            BreweryName = item.Brewery?.Name,
            StyleId = item.StyleId,
            StyleName = item.Style?.Name,
            IsDeleted = item.IsDeleted,           
            BeerRatings = item.BeerRatings.Select(br=>br.GetDTO()).ToList()
        };

        internal static Beer GetModel(this BeerDTO item)
            => item == null ? null : new Beer
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                ABV = item.ABV,
                Milliters = item.Milliters,
                IsDeleted = item.IsDeleted,
                StyleId = item.StyleId,
                BreweryId = item.BreweryId,
                BeerRatings = item.BeerRatings.Select(br => br.GetModel()).ToList()
                //TODO:  da se dopishat kolekciq review
            };

        public static object GetModelAsObject(this BeerDTO item)
            => item == null ? null :
                new
                {
                    Name = item.Name,
                    ABV = item.ABV,
                    Description = item.Description,
                    Milliters = item.Milliters,
                    StyleName = item.StyleName,
                    BreweryName = item.BreweryName,
                    Rating = item.Rating
                };
    }
}
