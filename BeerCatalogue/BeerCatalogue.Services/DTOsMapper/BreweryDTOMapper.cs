﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using System.Linq;

namespace BeerCatalogue.Services.DTOsMapper
{
    public static class BreweryDTOMapper
    {
        internal static BreweryDTO GetDTO(this Brewery item)
        => item == null ? null : new BreweryDTO
        {
            Id = item.Id,
            Name = item.Name,
            IsDeleted = item.IsDeleted,
            CountryId = item.CountryId,
            CountryName = item.Country?.Name,
            Beers = item.Beers.Select(b=>b.GetDTO()).ToList()
        };

        internal static Brewery GetModel(this BreweryDTO item)
            => item == null ? null : new Brewery
            {
                Id = item.Id,
                Name = item.Name,
                IsDeleted = item.IsDeleted,
                CountryId = item.CountryId,
                Beers = item.Beers.Select(b => b.GetModel()).ToList()
            };

        public static object GetModelAsObject(this BreweryDTO item)
            => item == null ? null :
                new
                {
                    Name = item.Name,
                    CountryName = item.CountryName,
                    Beers = item.Beers.Select(r => new
                    {
                        Name = r.Name,
                        ABV = r.ABV
                    })
                };
    }
}
