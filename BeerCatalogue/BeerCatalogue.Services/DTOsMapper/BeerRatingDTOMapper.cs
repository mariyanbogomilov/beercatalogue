﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;

namespace BeerCatalogue.Services.DTOsMapper
{
    public static class BeerRatingDTOMapper
    {
        internal static BeerRatingDTO GetDTO(this BeerRating item)
            => item == null ? null : new BeerRatingDTO
            {
                Rating = item.Rating,
                //Username = item.User?.Username,
                UserId = item.UserId
            };

        internal static BeerRating GetModel(this BeerRatingDTO item)
            => item == null ? null : new BeerRating
            {
                Rating = item.Rating, 
            };
    }
}
