﻿using BeerCatalogue.Models;
using BeerCatalogue.Services.DTOsEntities;
using System.Linq;

namespace BeerCatalogue.Services.DTOsMapper
{
    public static class CountryDTOMapper
    {
        internal static CountryDTO GetDTO(this Country item)
            => item == null ? null : new CountryDTO
            {
                Id=item.Id,
                Name = item.Name,
                IsDeleted = item.IsDeleted,
                Breweries = item.Breweries.Select(br=>br.GetDTO()).ToList()
            };

        internal static Country GetModel(this CountryDTO item)
            => item == null ? null : new Country
            {
                Id = item.Id,
                Name =item.Name,
                IsDeleted = item.IsDeleted,
                Breweries = item.Breweries.Select(br => br.GetModel()).ToList()
            };

        public static object GetModelAsObject(this CountryDTO item)
            => item == null ? null :
                new
                {
                    Name = item.Name,
                    Breweries = item.Breweries.Select(r => new
                    {
                        Name = r.Name
                    })
                };

    }
}
