﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> GetCountry(int id);
        //int GetPageCount(int countriesPerPage);
        //Task<IEnumerable<CountryDTO>> GetAllCountries(int currentPage, int pageSize);
        Task<IEnumerable<CountryDTO>> GetAllCountries();
        Task<CountryDTO> Create(CountryDTO countryDTO);
        Task<CountryDTO> Update(int id, CountryDTO countryDTO);
        Task<bool> Delete(int id);
    }
}
