﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IWishlistService
    {
        Task<IEnumerable<BeerDTO>> GetAllBeers(string username);
        Task<WishlistDTO> Create(WishlistDTO wishlistDTO);
        Task<bool> Remove(int id, string username);
    }
}
