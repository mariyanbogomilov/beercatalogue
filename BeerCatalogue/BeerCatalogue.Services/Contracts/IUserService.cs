﻿using BeerCatalogue.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IUserService
    {
        Task<IList<User>> GetAllUsers();
    }
}
