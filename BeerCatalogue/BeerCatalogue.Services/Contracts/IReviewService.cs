﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IReviewService
    {
        Task<IEnumerable<ReviewDTO>> GetAllReviews(int id);
        Task<bool> Delete(int beerId, int userId);
        Task<ReviewDTO> Create(ReviewDTO reviewDTO);
        Task<IEnumerable<ReviewDTO>> GetReviewsForUser(int id);
        //TODO:
        //ReviewDTO Update(int id, ReviewDTO reviewDTO);
        //public bool Delete(int id);
    }
}
