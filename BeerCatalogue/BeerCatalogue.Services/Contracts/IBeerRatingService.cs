﻿using BeerCatalogue.Services.DTOsEntities;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IBeerRatingService
    {
        Task<BeerRatingDTO> AddRating(BeerRatingDTO beerRatingDTO);
    }
}
