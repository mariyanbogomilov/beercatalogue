﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IStyleService
    {
        Task<StyleDTO> GetStyle(int id);
        Task<StyleDTO> CreateStyle(StyleDTO styleDTO);
        Task<IEnumerable<StyleDTO>> GetAllStyles();
        Task<StyleDTO> UpdateStyle(int id, StyleDTO styleDTO);
        Task<bool> Delete(int id);
    }
}
