﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> CreateBeer(BeerDTO beerDTO);
        Task<BeerDTO> GetBeer(int id);
        Task<IEnumerable<BeerDTO>> GetBeers();
        Task<BeerDTO> UpdateBeer(int id, BeerDTO beerDTO);
        Task<bool> Delete(int id);
        Task<IEnumerable<BeerDTO>> Filter(string type, string name);
        Task<IEnumerable<BeerDTO>> SortBy(string type, string order);
        Task<IEnumerable<BeerDTO>> SearchBeerByName(string searchString);
        Task<IEnumerable<BeerDTO>> BeersByBrewery(int breweryId);
        Task<IEnumerable<BeerDTO>> TopRatedBeers();
    }
}
