﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IDranklistService
    {
        Task<IEnumerable<BeerDTO>> GetAllBeers(string username);
        Task<DranklistDTO> Create(DranklistDTO wishlistDTO);
        Task<bool> Remove(int id, string username);
    }
}
