﻿using BeerCatalogue.Services.DTOsEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Contracts
{
    public interface IBreweryService
    {
        Task<BreweryDTO> GetBrewery(int id);
        Task<IEnumerable<BreweryDTO>> GetAllBreweries();
        Task<IEnumerable<BreweryDTO>> GetBreweriesForCountry(int id);
        Task<BreweryDTO> Create(BreweryDTO breweryDTO);
        Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO);
        Task<bool> Delete(int id);
    }
}
