﻿using BeerCatalogue.Services.DTOsEntities.Abstract;
using System.Collections.Generic;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class BreweryDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public ICollection<BeerDTO> Beers { get; set; } = new List<BeerDTO>();
    }
}
