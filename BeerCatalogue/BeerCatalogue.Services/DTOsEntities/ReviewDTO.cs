﻿using BeerCatalogue.Services.DTOsEntities.Abstract;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class ReviewDTO : BaseDTO
    {
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Comment { get; set; }
    }
}
