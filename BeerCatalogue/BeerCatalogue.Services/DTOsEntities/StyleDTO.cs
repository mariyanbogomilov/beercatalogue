﻿using BeerCatalogue.Services.DTOsEntities.Abstract;
using System.Collections.Generic;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class StyleDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<BeerDTO> Beers { get; set; } = new List<BeerDTO>();
    }
}
