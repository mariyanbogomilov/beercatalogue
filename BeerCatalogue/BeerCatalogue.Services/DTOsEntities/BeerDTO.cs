﻿using BeerCatalogue.Services.DTOsEntities.Abstract;
using System.Collections.Generic;
using System.Linq;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class BeerDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ABV { get; set; }
        public float Milliters { get; set; }
        public int BreweryId { get; set; }
        public double Rating
        {
            get
            {
                if (this.BeerRatings.Count != 0 )
                {
                    return this.BeerRatings.Average(x => x.Rating);
                }
                return 0;
            }
        }
        public string BreweryName { get; set; }
        public string StyleName { get; set; }
        public int StyleId { get; set; }
        public ICollection<ReviewDTO> Reviews { get; set; } = new List<ReviewDTO>();
        public ICollection<BeerRatingDTO> BeerRatings { get; set; } = new List<BeerRatingDTO>();

    }
}
