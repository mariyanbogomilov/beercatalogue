﻿using BeerCatalogue.Services.DTOsEntities.Abstract;
using System.Collections.Generic;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class CountryDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<BreweryDTO> Breweries { get; set; } = new List<BreweryDTO>();
    }
}
