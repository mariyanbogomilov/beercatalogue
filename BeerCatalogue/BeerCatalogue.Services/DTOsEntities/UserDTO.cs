﻿using System.Collections.Generic;

namespace BeerCatalogue.Services.DTOsEntities
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<WishlistDTO> Wishlists { get; set; } = new List<WishlistDTO>();
        public ICollection<ReviewDTO> Reviews { get; set; } = new List<ReviewDTO>();
        public ICollection<DranklistDTO> Dranklist { get; set; } = new List<DranklistDTO>();
        public ICollection<BeerRatingDTO> BeerRatings { get; set; } = new List<BeerRatingDTO>();
    }
}
