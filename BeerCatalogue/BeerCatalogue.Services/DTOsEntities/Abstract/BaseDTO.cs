﻿using System;

namespace BeerCatalogue.Services.DTOsEntities.Abstract
{
    public abstract class BaseDTO 
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
