﻿namespace BeerCatalogue.Services.DTOsEntities
{
    public class WishlistDTO
    {
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
