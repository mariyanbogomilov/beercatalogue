﻿using System;

namespace BeerCatalogue.Services.DTOsProvider.Contract
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
