﻿using BeerCatalogue.Services.DTOsProvider.Contract;
using System;

namespace BeerCatalogue.Services.DTOsProviders
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime()=>DateTime.UtcNow;
    }
}
