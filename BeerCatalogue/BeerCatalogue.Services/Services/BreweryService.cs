﻿using BeerCatalogue.Database;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext dbContext;

        public BreweryService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext dbContext)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<BreweryDTO> GetBrewery(int id)
        {
            var brewery = await this.dbContext.Breweries
                .Include(b => b.Country)
                .Include(b => b.Beers)
                .Where(b => !b.IsDeleted)
                .FirstOrDefaultAsync(b => b.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var breweryDTO = new BreweryDTO
            {
                Id = brewery.Id,
                Name = brewery.Name,
                IsDeleted = brewery.IsDeleted,
                CountryId = brewery.CountryId,
                CountryName = brewery.Country?.Name,
                Beers = brewery.Beers.Where(b => b.IsDeleted == false)
                .Select(b => b.GetDTO()).ToList()
            };

            return breweryDTO;
        }

        public async Task<IEnumerable<BreweryDTO>> GetBreweriesForCountry(int id)
        {
            var breweries = await this.dbContext.Breweries
                .Include(br => br.Country)
                .Include(br => br.Beers)
                .Where(br => br.IsDeleted == false && br.CountryId == id)
                .Select(b => b.GetDTO()).ToListAsync();

            return breweries;
        }
        public async Task<IEnumerable<BreweryDTO>> GetAllBreweries()
        {
            var breweries =await this.dbContext.Breweries
                .Include(br => br.Country)
                .Include(br => br.Beers)
                .Where(br => br.IsDeleted == false)
                .Select(b => b.GetDTO()).ToListAsync();

            return breweries;
        }

        public async Task<BreweryDTO> Create(BreweryDTO breweryDTO)
        {
            if (this.dbContext.Breweries.Any(b => b.Name == breweryDTO.Name))
            {
                throw new ArgumentException();
            }

            var country = await this.dbContext.Countries
                .Where(c => c.IsDeleted == false)
                .FirstOrDefaultAsync(c => c.Id == breweryDTO.CountryId);

            breweryDTO.CountryId = country.Id;
            await this.dbContext.Breweries.AddAsync(breweryDTO.GetModel());
            await this.dbContext.SaveChangesAsync();

            return breweryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var brewery =await this.dbContext.Breweries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Id == id);

                brewery.IsDeleted = true;
                brewery.DeletedOn = this.dateTimeProvider.GetDateTime();

                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO)
        {
            var brewery = await this.dbContext.Breweries
                .Include(x => x.Country)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var country = this.dbContext.Countries
                .Where(country => country.IsDeleted == false)
                .FirstOrDefault(country => country.Id == breweryDTO.CountryId);

            brewery.Name = breweryDTO.Name;
            //brewery.CountryId = this.dbContext
            //    .Countries
            //    .FirstOrDefaultAsync(c => c.Name == breweryDTO.CountryName).Id;

            brewery.CountryId = country.Id;

            brewery.ModifiedOn = this.dateTimeProvider.GetDateTime();
            await this.dbContext.SaveChangesAsync();

            return breweryDTO;
        }
    }
}
