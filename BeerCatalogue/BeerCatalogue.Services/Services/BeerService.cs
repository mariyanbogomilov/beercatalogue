﻿using BeerCatalogue.Database;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class BeerService : IBeerService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext dbContext;

        public BeerService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext dbContext)
        {
            this.dateTimeProvider = dateTimeProvider;
            this.dbContext = dbContext;
        }

        public async Task<BeerDTO> CreateBeer(BeerDTO beerDTO)
        {
            var brewery =await this.dbContext.Breweries
                .Where(br => br.IsDeleted == false)
                .FirstOrDefaultAsync(br => br.Name == beerDTO.BreweryName);

            var style =await this.dbContext.Styles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(s => s.Name == beerDTO.StyleName);

            beerDTO.StyleId = style.Id;
            beerDTO.BreweryId = brewery.Id;

            await this.dbContext.Beers.AddAsync(beerDTO.GetModel());
            await this.dbContext.SaveChangesAsync();
            return beerDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var beer =await this.dbContext.Beers
                    .Where(s => s.IsDeleted == false)
                    .FirstOrDefaultAsync(s => s.Id == id);

                beer.IsDeleted = true;
                beer.DeletedOn = dateTimeProvider.GetDateTime();

                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<BeerDTO>> BeersByBrewery(int breweryId)
        {
            var beers = await this.dbContext.Beers
                .Where(b => b.IsDeleted == false)
                .Where(b => b.BreweryId == breweryId)
                .ToListAsync();

            var beersDto = beers.Select(b => b.GetDTO());

            return beersDto;
        }

        public async Task<IEnumerable<BeerDTO>> SearchBeerByName(string searchString)
        {
            if (String.IsNullOrWhiteSpace(searchString))
            {
                return this.dbContext.Beers
                    .Where(b => b.IsDeleted == false)
                    .Select(b => b.GetDTO());
            }

            var beers = await this.dbContext.Beers.Where(b => b.IsDeleted == false)
                .Where(b => b.Name.Contains(searchString))
                .Select(b => b.GetDTO())
                .ToListAsync();

            return beers;
        }

        public async Task<IEnumerable<BeerDTO>> TopRatedBeers()
        {
           var beers = await this.dbContext.Beers
                        .Include(b => b.BeerRatings)
                        .OrderByDescending(b => b.BeerRatings.Average(b => b.Rating))
                        .Take(3)
                        .Select(b => b.GetDTO()).ToListAsync();

            return beers;
        }

        public async Task<IEnumerable<BeerDTO>> Filter(string type, string name)
        {
            if (type == "style")
            {
                var style =await this.dbContext.Styles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(s => s.Name == name);

                var beers = await this.dbContext.Beers
                    .Include(b => b.Brewery)
                    .Include(b=> b.Style)
                    .Include(b => b.Brewery.Country)
                    .Include(b => b.BeerRatings)
                    .Where(b => !b.IsDeleted && b.StyleId == style.Id)
                    .Select(b => b.GetDTO()).ToListAsync();

                return beers;
            }
            else if (type == "country")
            {
                var beers =await this.dbContext.Beers
                    .Include(b => b.Brewery)
                    .Include(b => b.Style)
                    .Include(b => b.Brewery.Country)
                    .Include(b => b.BeerRatings)
                    .Where(b => !b.IsDeleted && b.Brewery.Country.Name == name)
                    .Select(b => b.GetDTO()).ToListAsync();

                return beers;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public async Task<BeerDTO> GetBeer(int id)
        {
            var beer = await this.dbContext.Beers
            .Include(b => b.Brewery).ThenInclude(br => br.Country)
            .Include(b => b.Style)
            .Include(b => b.BeerRatings)
            .Where(b => b.IsDeleted == false)
            .FirstOrDefaultAsync(b => b.Id == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }
            var beerDTO = new BeerDTO
            {
                Id = beer.Id,
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                Milliters = beer.Milliters,
                BreweryId = beer.BreweryId,
                BreweryName = beer.Brewery?.Name,
                StyleId = beer.StyleId,
                StyleName = beer.Style?.Name,
                IsDeleted = beer.IsDeleted,
                BeerRatings = beer.BeerRatings.Select(br => br.GetDTO()).ToList()
            };

            return beerDTO;
        }

        public async Task<IEnumerable<BeerDTO>> GetBeers()
            => await this.dbContext.Beers
                .Include(b => b.Brewery).ThenInclude(br => br.Country)
                .Include(b => b.Style)
                .Include(b=>b.BeerRatings)
                .Where(b => b.IsDeleted == false)
                .Select(beer => beer.GetDTO()).ToListAsync();

        public async Task<BeerDTO> UpdateBeer(int id, BeerDTO beerDTO)
        {
            var beer = await this.dbContext.Beers
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            beer.Name = beerDTO.Name;
            beer.ABV = beerDTO.ABV;
            beer.Milliters = beerDTO.Milliters;
            beer.ModifiedOn = dateTimeProvider.GetDateTime();
            await this.dbContext.SaveChangesAsync();
            return beerDTO;
        }
        public async Task<IEnumerable<BeerDTO>> SortBy(string type, string order)
        {
            List<BeerDTO> beers = null;
            if (type == "abv")
            {
                if (order == "desc")
                {
                    beers = await this.dbContext.Beers
                       .Include(b => b.Brewery)
                       .Include(b => b.Style)
                       .Include(b => b.Brewery.Country)
                       .Include(b => b.BeerRatings)
                       .Include(b => b.BeerRatings)
                       .OrderByDescending(b => b.ABV)
                       .Select(b => b.GetDTO()).ToListAsync();
                }
                else
                {
                    beers = await this.dbContext.Beers
                       .Include(b => b.Brewery)
                       .Include(b => b.Style)
                       .Include(b => b.Brewery.Country)
                       .Include(b => b.BeerRatings)
                       .OrderBy(b => b.ABV)
                       .Select(b => b.GetDTO()).ToListAsync();
                }

                return beers;
            }
            else if (type == "name")
            {
                if (order == "desc")
                {
                    beers = await this.dbContext.Beers
                       .Include(b => b.Brewery)
                       .Include(b => b.Style)
                       .Include(b => b.Brewery.Country)
                       .Include(b => b.BeerRatings)
                       .Include(b => b.BeerRatings)
                       .OrderByDescending(b => b.Name)
                       .Select(b => b.GetDTO()).ToListAsync();
                }
                else
                {
                    beers = await this.dbContext.Beers
                       .Include(b => b.Brewery)
                       .Include(b => b.Style)
                       .Include(b => b.Brewery.Country)
                       .Include(b => b.BeerRatings)
                       .OrderBy(b => b.Name)
                       .Select(b => b.GetDTO()).ToListAsync();
                }

                return beers;
            }
            else if (type == "rating")
            {
                if (order == "desc")
                {
                     beers = await this.dbContext.Beers
                        .Include(b => b.Brewery)
                        .Include(b => b.Style)
                        .Include(b => b.Brewery.Country)
                        .Include(b => b.BeerRatings)
                        .Include(b=>b.BeerRatings)
                        .OrderByDescending(b => b.BeerRatings.Average(b => b.Rating))
                        .Select(b => b.GetDTO()).ToListAsync();
                }
                else
                {
                     beers =await this.dbContext.Beers
                        .Include(b => b.Brewery)
                        .Include(b => b.Style)
                        .Include(b => b.Brewery.Country)
                        .Include(b => b.BeerRatings)
                        .OrderBy(b => b.BeerRatings.Average(b => b.Rating))
                        .Select(b => b.GetDTO()).ToListAsync();
                }

                return beers;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

    }
}
