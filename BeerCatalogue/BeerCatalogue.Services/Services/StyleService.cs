﻿

using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class StyleService : IStyleService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext dbContext;

        public StyleService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext dbContext)
        {
            this.dateTimeProvider = dateTimeProvider;
            this.dbContext = dbContext;
        }
        public async Task<StyleDTO> CreateStyle(StyleDTO styleDTO)
        {
            var style = new Style
            {
                Id = styleDTO.Id,
                Name = styleDTO.Name,
                Description = styleDTO.Description,
                CreatedOn = this.dateTimeProvider.GetDateTime(),
                IsDeleted = false
            };
            await this.dbContext.Styles.AddAsync(style);
            await this.dbContext.SaveChangesAsync();
            return styleDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var style =await this.dbContext.Styles
                    .Where(s => s.IsDeleted == false)
                    .FirstOrDefaultAsync(s => s.Id == id);

                style.IsDeleted = true;
                style.DeletedOn = dateTimeProvider.GetDateTime();

                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<StyleDTO>> GetAllStyles()
            => await this.dbContext.Styles
            .Include(s=>s.Beers)
            .Where(x => x.IsDeleted == false)
            .Select(x => x.GetDTO()).ToListAsync();

        public async Task<StyleDTO> GetStyle(int id)
        {
            var style = await this.dbContext.Styles
                .Include(c => c.Beers)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            var styleDTO = new StyleDTO
            {
                Id = style.Id,
                Name = style.Name,
                Description = style.Description,
                Beers = style.Beers
                .Where(b=>!b.IsDeleted)
                .Select(br => br.GetDTO()).ToList()
            };

            return styleDTO;
        }

        public async Task<StyleDTO> UpdateStyle(int id, StyleDTO styleDTO)
        {
            var style =await this.dbContext.Styles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            style.Name = styleDTO.Name;
            style.Description = styleDTO.Description;
            style.ModifiedOn = dateTimeProvider.GetDateTime();
            await this.dbContext.SaveChangesAsync();
            return styleDTO;
        }
    }
}
