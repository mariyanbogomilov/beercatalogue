﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    
    public class BeerRatingService : IBeerRatingService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext context;

        public BeerRatingService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext context)
        {
            this.dateTimeProvider = dateTimeProvider;
            this.context = context;
        }

        public async Task<BeerRatingDTO> AddRating(BeerRatingDTO beerRatingDTO)
        {
            var beer = await this.context.Beers
                .Where(b => b.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Name == beerRatingDTO.BeerName);

            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.UserName == beerRatingDTO.Username);

            if (beer == null || user == null)
            {
                throw new ArgumentNullException("Beer or user does not exist!");
            }

            var beerRating = new BeerRating
            {
                Beer = beer,
                User = user,
                Rating = beerRatingDTO.Rating
            };

            await this.context.BeerRatings.AddAsync(beerRating);
            await this.context.SaveChangesAsync();

            return beerRatingDTO;
        }
    }
}
