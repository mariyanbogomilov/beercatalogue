﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class CountryService : ICountryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext dbContext;

        public CountryService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext dbContext)
        {
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider)); ;
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext)); ;
        }

        public async Task<CountryDTO> GetCountry(int id)
        {
            var country = await this.dbContext.Countries
                .Include(c => c.Breweries)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = new CountryDTO
            {
                Id = country.Id,
                Name = country.Name,
                IsDeleted = country.IsDeleted,
                Breweries = country.Breweries.Where(br => br.IsDeleted == false)
                .Select(br => br.GetDTO()).ToList()
            };

            return countryDTO;
        }

        //public int GetPageCount(int countriesPerPage)
        //{
        //    var count = this.dbContext.Countries.Count();
        //
        //    var totalPages = (count - 1) / countriesPerPage + 1;
        //
        //    return totalPages;
        //}

        //public async Task<IEnumerable<CountryDTO>> GetAllCountries(int currentPage, int pageSize)
        //{
        //    var countries =await this.dbContext.Countries
        //        .Include(c=>c.Breweries)
        //        .Where(x => x.IsDeleted == false)
        //        .Select(x => new CountryDTO
        //        {
        //            Id = x.Id,
        //            Name = x.Name,
        //            CreatedOn = x.CreatedOn,
        //            Breweries = x.Breweries.Select(x=>x.GetDTO()).ToList()
        //        }).ToListAsync();
        //
        //    var result = currentPage == 1 ? countries.Take(pageSize) : countries.Skip((currentPage - 1) * pageSize).Take(pageSize);
        //
        //    return result;
        //}

        public async Task<IEnumerable<CountryDTO>> GetAllCountries()
        {
            var countries = await this.dbContext.Countries
                .Include(br => br.Breweries)
                .Where(c => c.IsDeleted == false)
                .Select(c => new CountryDTO
                {
                    Id = c.Id,
                    Name = c.Name,
                    CreatedOn = c.CreatedOn,
                    Breweries = c.Breweries.Where(br => br.IsDeleted == false)
                    .Select(x => x.GetDTO()).ToList()
                }).ToListAsync();

            return countries;
        }

        public async Task<CountryDTO> Create(CountryDTO countryDTO)
        {
            if (this.dbContext.Countries.Any(country => country.Name == countryDTO.Name))
            {
                throw new ArgumentException("Country already exists");
            }

            var country = new Country
            {
                Id = countryDTO.Id,
                Name = countryDTO.Name,
                CreatedOn = this.dateTimeProvider.GetDateTime(),
                IsDeleted = false
            };

            await this.dbContext.Countries.AddAsync(country);
            await this.dbContext.SaveChangesAsync();
            return countryDTO;
        }

        public async Task<CountryDTO> Update(int id, CountryDTO countryDTO)
        {
            var country = this.dbContext.Countries
                .Where(c => c.IsDeleted == false)
                .FirstOrDefault(x => x.Id == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            country.Name = countryDTO.Name;
            country.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this.dbContext.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var country = await this.dbContext.Countries
                    .Where(c => c.IsDeleted == false)
                    .FirstOrDefaultAsync(x => x.Id == id);

                country.IsDeleted = true;
                country.DeletedOn = this.dateTimeProvider.GetDateTime();
                await this.dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
