﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class WishlistService : IWishlistService
    {
        private readonly BeerCatalogueContext context;

        public WishlistService(BeerCatalogueContext context)
        {
            this.context = context;
        }


        public async Task<WishlistDTO> Create(WishlistDTO wishlistDTO)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(user => user.Id == wishlistDTO.UserId);

            var beer = await this.context.Beers
                .Where(b => b.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Id == wishlistDTO.BeerId);

            if (beer == null || user == null)
            {
                throw new ArgumentException("Beer or user does not exist!");
            }

            var wishlist = new Wishlist
            {
                BeerId = beer.Id,
                UserId = user.Id,
            };

            if (this.context.Wishlists.Any(wl => wl.UserId == wishlist.UserId && wl.BeerId == wishlist.BeerId))
            {
                throw new ArgumentException();
            }

            await this.context.Wishlists.AddAsync(wishlist);
            await this.context.SaveChangesAsync();

            return wishlistDTO;

        }

        public async Task<IEnumerable<BeerDTO>> GetAllBeers(string username)
        {
            var user = await this.context.Users
                .Include(user => user.Wishlists)
                .ThenInclude(wl => wl.Beer)
                .FirstOrDefaultAsync(u => u.UserName == username);

            var userBeersInWishlist = user.Wishlists.Select(b => b.Beer.GetDTO());

            return userBeersInWishlist;
        }

        public async Task<bool> Remove(int id, string username)
        {
            try
            {
                var user = await this.context.Users
                .Include(user => user.Wishlists)
                .ThenInclude(wl => wl.Beer)
                .FirstOrDefaultAsync(user => user.UserName == username);

                var beerToRemove = await this.context.Beers
                    .FirstOrDefaultAsync(b => b.Id == id);

                var wishlist = user.Wishlists
                    .Where(b => b.BeerId == beerToRemove.Id)
                    .First();

                user.Wishlists.Remove(wishlist);

                await this.context.SaveChangesAsync();

                return true;
            }
            catch (System.Exception)
            {

                return false;
            }
        }
    }
}
