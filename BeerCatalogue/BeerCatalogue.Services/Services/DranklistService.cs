﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class DranklistService : IDranklistService
    {
        private readonly BeerCatalogueContext context;

        public DranklistService(BeerCatalogueContext context)
        {
            this.context = context;
        }

        public async Task<DranklistDTO> Create(DranklistDTO dranklistDto)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(user => user.Id == dranklistDto.UserId);

            var beer = await this.context.Beers
                .Where(b => b.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Id == dranklistDto.BeerId);

            if (beer == null || user == null)
            {
                throw new ArgumentException("Beer or user does not exist!");
            }

            var dranklist = new Dranklist
            {
                BeerId = beer.Id,
                UserId = user.Id,
            };

            if (this.context.Dranklists.Any(dl => dl.UserId == dranklist.UserId && dl.BeerId == dranklist.BeerId))
            {
                throw new ArgumentException();
            }

            await this.context.Dranklists.AddAsync(dranklist);
            await this.context.SaveChangesAsync();

            return dranklistDto;

        }

        public async Task<IEnumerable<BeerDTO>> GetAllBeers(string username)
        {
            var user = await this.context.Users
                .Include(user => user.Dranklist)
                .ThenInclude(dl => dl.Beer)
                .FirstOrDefaultAsync(u => u.UserName == username);

            var userBeersInDranklist = user.Dranklist.Select(b => b.Beer.GetDTO());

            return userBeersInDranklist;
        }

        public async Task<bool> Remove(int id, string username)
        {
            try
            {
                var user = await this.context.Users
                .Include(user => user.Dranklist)
                .ThenInclude(dl => dl.Beer)
                .FirstOrDefaultAsync(user => user.UserName == username);

                var beerToRemove = await this.context.Beers
                    .FirstOrDefaultAsync(b => b.Id == id);

                var dranklist = user.Dranklist
                    .Where(b => b.BeerId == beerToRemove.Id)
                    .First();

                user.Dranklist.Remove(dranklist);

                await this.context.SaveChangesAsync();

                return true;
            }
            catch (System.Exception)
            {

                return false;
            }
        }
    }
}
