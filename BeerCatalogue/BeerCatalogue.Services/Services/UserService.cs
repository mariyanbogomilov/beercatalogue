﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class UserService : IUserService
    {
        private readonly BeerCatalogueContext context;
        private readonly UserManager<User> userManager;

        public UserService(BeerCatalogueContext context, UserManager<User> userManager)
        {
            this.userManager = userManager;
            this.context = context;
        }

        public async Task<IList<User>> GetAllUsers()
        {
            var regularUsers = new List<User>();
            var users = this.context.Users.ToList();
            foreach (var u in users)
            {
                if (await this.userManager.IsInRoleAsync(u, "User"))
                {
                    regularUsers.Add(u);
                }
            }
            
            return regularUsers;
        }
    }
}
