﻿using BeerCatalogue.Database;
using BeerCatalogue.Models;
using BeerCatalogue.Services.Contracts;
using BeerCatalogue.Services.DTOsEntities;
using BeerCatalogue.Services.DTOsProvider.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerCatalogue.Services.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerCatalogueContext context;

        public ReviewService(IDateTimeProvider dateTimeProvider, BeerCatalogueContext context)
        {
            this.dateTimeProvider = dateTimeProvider;
            this.context = context;
        }

        public async Task<ReviewDTO> Create(ReviewDTO reviewDTO)
        {
            var beer = await this.context.Beers
                .Where(b => b.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.Name == reviewDTO.BeerName);

            if (beer == null)
            {
                throw new ArgumentException("Beer or user does not exist!");
            }
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.UserName == reviewDTO.Username);

            if (user == null)
            {
                throw new ArgumentException("Beer or user does not exist!");
            }
            var review = new Review
            {
                Beer = beer,
                User = user,
                Comment = reviewDTO.Comment,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await this.context.Reviews.AddAsync(review);
            await this.context.SaveChangesAsync();

            return reviewDTO;
        }

        public async Task<bool> Delete(int beerId,int userId)
        {
            try
            {

                var review = this.context.Reviews
                    .Where(s => s.UserId == userId && s.BeerId == beerId).FirstOrDefault();

                this.context.Reviews.Remove(review);
                await this.context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<ReviewDTO>> GetAllReviews(int id)
        {
            var reviews = await this.context.Reviews
                .Where(r => r.IsDeleted == false && r.BeerId == id)
                .Select(r => new ReviewDTO
                {
                    BeerId = r.BeerId,
                    UserId = r.UserId,
                    Username = r.User.UserName,
                    Comment = r.Comment,
                })
                .ToListAsync();

            return reviews;
        }

        public async Task<IEnumerable<ReviewDTO>> GetReviewsForUser(int id)
        {
            var reviews = await this.context.Reviews
                .Where(r => r.IsDeleted == false && r.UserId == id)
                .Select(r => new ReviewDTO
                {
                    BeerId = r.BeerId,
                    UserId = r.UserId,
                    Username = r.User.UserName,
                    BeerName = r.Beer.Name,
                    Comment = r.Comment,
                })
                .ToListAsync();

            return reviews;
        }
    }
}
