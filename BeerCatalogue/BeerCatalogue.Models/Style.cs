﻿using BeerCatalogue.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogue.Models
{
    public class Style : BaseEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
