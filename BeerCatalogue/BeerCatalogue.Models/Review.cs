﻿using BeerCatalogue.Models.Abstract;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogue.Models
{
    public class Review : BaseEntity
    {
        [Required]
        [MinLength(10)]
        public string Comment { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
