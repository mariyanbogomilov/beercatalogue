﻿namespace BeerCatalogue.Models
{
    public class Wishlist
    {
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
