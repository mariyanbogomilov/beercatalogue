﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogue.Models
{
    public class BeerRating 
    {
        [Required]
        [Range(1,5)]
        public int Rating { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
