﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BeerCatalogue.Models
{
    public class User : IdentityUser<int>
    {
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<BeerRating> BeerRatings { get; set; } = new List<BeerRating>();
        public ICollection<Wishlist> Wishlists { get; set; } = new List<Wishlist>();
        public ICollection<Dranklist> Dranklist { get; set; } = new List<Dranklist>();
    }
}
