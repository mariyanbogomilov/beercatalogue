﻿using BeerCatalogue.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogue.Models
{
    public class Beer : BaseEntity
    {
        [Key]
        public int Id { get; set; }

        [MinLength(5)]
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        [StringLength(750, MinimumLength = 10)]
        public string Description { get; set; }

        [Required]
        public string ABV { get; set; }

        [Required]
        public float Milliters { get; set; }
        public Brewery Brewery { get; set; }
        public int BreweryId { get; set; }
        public Style Style { get; set; }
        public int StyleId { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<BeerRating> BeerRatings { get; set; } = new List<BeerRating>();
        public ICollection<Wishlist> Wishlist { get; set; } = new List<Wishlist>();
        public ICollection<Dranklist> Dranklist { get; set; } = new List<Dranklist>();
    }
}
