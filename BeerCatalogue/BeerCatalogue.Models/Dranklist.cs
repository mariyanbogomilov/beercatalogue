﻿namespace BeerCatalogue.Models
{
    public class Dranklist
    {
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
