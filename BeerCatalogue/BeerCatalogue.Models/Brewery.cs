﻿using BeerCatalogue.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerCatalogue.Models
{
    public class Brewery : BaseEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 5)]
        public string Name { get; set; }

        public Country Country { get; set; }
        public int CountryId { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
