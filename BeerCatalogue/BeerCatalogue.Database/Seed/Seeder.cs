﻿using BeerCatalogue.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BeerCatalogue.Database.Seed
{
    public static class Seeder
    {
        public static void Seed(this ModelBuilder builder)
        {

            builder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = 2, Name = "User", NormalizedName = "USER" },
                new Role() { Id = 3, Name = "Banned", NormalizedName = "BANNED" }
            );
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    Id =2,
                    Name = "Czech"
                },
                new Country
                {
                    Id = 3,
                    Name = "Germany"
                },
                new Country
                {
                    Id = 4,
                    Name = "Hungary"
                },
                new Country
                {
                    Id = 5,
                    Name = "Canada"
                },
                new Country
                {
                    Id = 6,
                    Name = "Finland"
                },
                new Country
                {
                    Id = 7,
                    Name = "Poland"
                },
                new Country
                {
                    Id = 8,
                    Name = "Spain"
                },
                new Country
                {
                    Id = 9,
                    Name = "Venezuela"
                },
                new Country
                {
                    Id = 10,
                    Name = "Lithuania"
                },
            };
            builder.Entity<Country>().HasData(countries);

            var styles = new List<Style>
            {
                new Style
                {
                    Id = 1,
                    Name = "Lager",
                    Description = "Lager is a type of beer conditioned at low temperature."
                },
                new Style
                {
                    Id = 2,
                    Name = "Fruit",
                    Description = "Fruit beer is beer made with fruit added as an adjunct or flavouring. "
                },
                new Style
                {
                    Id = 3,
                    Name = "Pilsner",
                    Description = "Pilsner (also pilsener or simply pils) is a type of pale lager. "
                },
                new Style
                {
                    Id = 4,
                    Name = "Pale Lager",
                    Description = "Pale lager is a very pale-to-golden-colored lager beer with a well-attenuated"
                },
                new Style
                {
                    Id = 5,
                    Name = "Stout",
                    Description = "Stout is a dark, top-fermented beer with a number of variations,"
                },
                new Style
                {
                    Id = 6,
                    Name = "Pale Ale",
                    Description = "The classic Pale Ale is generally a deep-golden to copper colored, hop-forward ale with a balanced malt profile. This style specifically represents all generic Pale Ales (sometime called International Pale Ale) which are marketed as such and which cannot be defined as a specific regional Pale Ale style such as the American Pale Ale."
                },
                new Style
                {
                    Id = 7,
                    Name = "IPA - Brut",
                    Description = "The Brut India Pale Ale (IPA) is a very pale to light golden, very dry, highly effervescent variant of American IPA, usually highly hopped with aromatic hops, but with far less actual bitterness. Enzymes are used in the mash and/or fermenter along with highly fermentable wort and often adjuncts like rice and corn to achieve close to total attenuation, resulting in an absent residual malt sweetness. Hopped in a similar fashion to New England IPA, but without sweetness."
                },
                new Style
                {
                    Id = 8,
                    Name = "IPA - English",
                    Description = "The English India Pale Ale (IPA) is a hoppy, moderately-strong, very well-attenuated pale golden to deep amber British ale with a dry finish and a hoppy aroma and flavor. Generally will have more finish hops and less fruitiness and/or caramel than British pale ales and bitter and has less hop intensity and a more pronounced malt flavor than typical American versions. The modern IPA style generally refers to American IPA"
                },
                new Style
                {
                    Id = 9,
                    Name = "Old Ale",
                    Description = "The Old Ale is an light amber to very dark reddish-brown colored English ale of moderate to fairly significant alcoholic strength, bigger than standard beers, though usually not as strong or rich as barleywine and often tilted towards a maltier balance. The predominant defining quality for this style is the impression of age, which can manifest itself in different ways (complexity, lactic, Brett, oxidation, leather, vinous qualities are some recurring examples)."
                }
            };
            builder.Entity<Style>().HasData(styles);

            var breweries = new List<Brewery>
            {
                new Brewery
                {
                    Id = 1,
                    Name = "Kamenitza AD",
                    CountryId = 1
                },
                new Brewery
                {
                    Id = 2,
                    Name = "Anheuser–Busch InBev",
                    CountryId =2
                },
                new Brewery
                {
                    Id = 3,
                    Name = "Heineken International",
                    CountryId = 3
                },
                new Brewery
                {
                    Id = 4,
                    Name = "Diageo",
                    CountryId =4
                },
                new Brewery
                {
                    Id = 5,
                    Name = "Amstel Brouwerij",
                    CountryId = 3
                },
                new Brewery
                {
                    Id = 6,
                    Name = "Astika",
                    CountryId = 1
                },
                new Brewery
                {
                    Id = 7,
                    Name = "Burgasko",
                    CountryId = 1
                },
                new Brewery
                {
                    Id = 8,
                    Name = "Schumacher brewery of Düsseldorf",
                    CountryId = 3
                },
                new Brewery
                {
                    Id = 9,
                    Name = "Stevens Point Brewery",
                    CountryId = 3
                }
            };
            builder.Entity<Brewery>().HasData(breweries);

            var beers = new List<Beer>
            {
                new Beer
                {
                    Id = 1,
                    Name = "Kamenitza",
                    ABV = "4.4%",
                    Description = "Light beer with an extract content of 10.2 ° P.",
                    Milliters = 0.5f,
                    StyleId = 1,
                    BreweryId = 1,
                },
                new Beer
                {
                    Id = 2,
                    Name = "Kamenitza Lemon Fresh",
                    ABV = "3.0%",
                    Milliters = 0.5f,
                    Description = "Light seasonal fruit beer with an extract content of 8.3 ° P.",
                    StyleId = 2,
                    BreweryId = 1,
                },
                new Beer
                {
                    Id = 3,
                    Name = "Stella Artois",
                    ABV = "5.2%",
                    Milliters = 0.5f,
                    Description = "A Belgian pilsner of between 4.8 and 5.2 percent ABV which was first brewed by Brouwerij Artois (the Artois Brewery) in Leuven, Belgium, in 1926. Since 2008, a 4.8 percent ABV version has also been sold in Britain, Ireland, Canada and Australia. Stella Artois is now owned by Interbrew International B.V. which is a subsidiary of the world's largest brewer, Anheuser-Busch InBev SA/NV.",
                    StyleId = 3,
                    BreweryId = 2
                },
                new Beer
                {
                    Id = 4,
                    Name = "Heineken",
                    ABV = "5%",
                    Milliters = 0.5f,
                    Description = "A pale lager beer with 5% alcohol by volume produced by the Dutch brewing company Heineken International. Heineken beer is sold in a green bottle with a red star.",
                    StyleId = 4,
                    BreweryId = 3
                },
                new Beer
                {
                    Id = 5,
                    Name = "Guinness Original",
                    ABV = "4.2%",
                    Milliters = 0.5f,
                    Description = "4.2 to 5.6% in the United States. 5% in Canada, and most of Europe; 4.2 or 4.3% ABV in Ireland and some European countries, 4.1% in Germany, 4.8% in Namibia and South Africa, and 6% in Australia and Japan.",
                    StyleId = 5,
                    BreweryId = 4
                },
                new Beer
                {
                    Id = 6,
                    Name = "Amstel",
                    ABV = "3.5%",
                    Milliters = 0.5f,
                    Description = "According to the Amstel website, Amstel beer is pure - filtered which creates a full-strength beer without the calories and carbohydrates.",
                    StyleId = 4,
                    BreweryId = 5
                },
                new Beer
                {
                    Id = 7,
                    Name = "Zagorka",
                    ABV = "6.0%",
                    Milliters = 0.5f,
                    Description = "A high-quality lager beer, the main ingredients of which are barley malt, water, hops and yeast. It is characterized by light golden color, moderate carbonation, fresh taste, with a slight aroma of malt and hops. Available on the market in glass bottles of 0.5 liters, as well as in PET bottles of 1 and 2 liters.",
                    StyleId = 1,
                    BreweryId = 6
                },
                new Beer
                {
                    Id = 8,
                    Name = "Altbier",
                    ABV = "6.5%",
                    Milliters = 0.75f,
                    Description = "A top fermented, lagered beer. It is brewed only in Düsseldorf and in the Lower Rhine region. Its origins lie in Westphalia, and there are still a few Altbier breweries in this region. Tastes range from mildly bitter and hoppy to exceptionally bitter. About ten breweries in the Düsseldorf region brew Altbier at 5–6.5% ABV.",
                    StyleId = 4,
                    BreweryId = 8
                },
                new Beer
                {
                    Id = 9,
                    Name = "Doppelbock",
                    ABV = "11.0%",
                    Milliters = 0.65f,
                    Description = "A very strong, very full bodied lager that uses dark coloured malts. 18–28° Plato, 8–12% ABV",
                    StyleId = 1,
                    BreweryId = 9
                }
            };
            builder.Entity<Beer>().HasData(beers);

            var hasher = new PasswordHasher<User>();
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.UserName = "admin123@admin.com";
            adminUser.NormalizedUserName = "ADMIN123@ADMIN.COM";
            adminUser.Email = "admin123@admin.com";
            adminUser.NormalizedEmail = "ADMIN123@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(adminUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                }
            );

            var regularUser = new User();
            regularUser.Id = 2;
            regularUser.UserName = "alice@alice.com";
            regularUser.NormalizedUserName = "ALICE@ALICE.COM";
            regularUser.Email = "alice@alice.com";
            regularUser.NormalizedEmail = "ALICE@ALICE.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "alice123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );
            // Bob
            regularUser = new User();
            regularUser.Id = 3;
            regularUser.UserName = "bob@bob.com";
            regularUser.NormalizedUserName = "BOB@BOB.COM";
            regularUser.Email = "bob@bob.com";
            regularUser.NormalizedEmail = "BOB@BOB.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "bob123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );

            var reviews = new List<Review>
            {
                new Review
                {
                    Comment = "This is exellent beer!",
                    BeerId = 7,
                    UserId = 3
                },
                new Review
                {
                    Comment = "I didn't really like it. Poor colour, bad taste.",
                    BeerId = 3,
                    UserId = 2
                },
                new Review
                {
                    Comment = "It's okay I guess. Very fruity aroma. Light sour, strong sweet taste. Fruity.",
                    BeerId = 2,
                    UserId = 3
                },
                new Review
                {
                    Comment = "Absolutely amazing!. One of the best Bulgarian beers.",
                    BeerId = 7,
                    UserId = 2
                }
            };
            builder.Entity<Review>().HasData(reviews);

            var ratings = new List<BeerRating>()
            {
                new BeerRating
                {
                    Rating = 3,
                    BeerId = 7,
                    UserId = 2,
                },
                new BeerRating
                {
                    Rating = 1,
                    BeerId = 7,
                    UserId = 3,
                },
                new BeerRating
                {
                    Rating = 5,
                    BeerId = 5,
                    UserId = 2,
                }
            };
            builder.Entity<BeerRating>().HasData(ratings);
        }
    }
}
