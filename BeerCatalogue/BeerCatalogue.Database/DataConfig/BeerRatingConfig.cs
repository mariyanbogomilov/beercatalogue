﻿using BeerCatalogue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerCatalogue.Database.DataConfig
{
    public class BeerRatingConfig : IEntityTypeConfiguration<BeerRating>
    {
        public void Configure(EntityTypeBuilder<BeerRating> builder)
        {
            builder.HasKey(br => new { br.BeerId, br.UserId });

            builder
                .HasOne(review => review.Beer)
                .WithMany(beer => beer.BeerRatings)
                .HasForeignKey(review => review.BeerId);

            builder
                .HasOne(review => review.User)
                .WithMany(user => user.BeerRatings)
                .HasForeignKey(review => review.UserId);
        }
    }
}
