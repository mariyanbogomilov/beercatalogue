﻿using BeerCatalogue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerCatalogue.Database.DataConfig
{
    public class WishlistConfig : IEntityTypeConfiguration<Wishlist>
    {
        public void Configure(EntityTypeBuilder<Wishlist> builder)
        {
            builder.HasKey(w => new { w.BeerId, w.UserId });

            builder.HasOne(w => w.Beer)
                .WithMany(w => w.Wishlist)
                .HasForeignKey(w => w.BeerId);

            builder.HasOne(w => w.User)
                .WithMany(w => w.Wishlists)
                .HasForeignKey(w => w.UserId);
        }
    }
}
