﻿using BeerCatalogue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerCatalogue.Database.DataConfig
{
    public class DranklistConfig : IEntityTypeConfiguration<Dranklist>
    {
        public void Configure(EntityTypeBuilder<Dranklist> builder)
        {
            builder.HasKey(d => new { d.BeerId, d.UserId });

            builder
                .HasOne(review => review.Beer)
                .WithMany(beer => beer.Dranklist)
                .HasForeignKey(review => review.BeerId);

            builder
                .HasOne(review => review.User)
                .WithMany(user => user.Dranklist)
                .HasForeignKey(review => review.UserId);
        }
    }
}
