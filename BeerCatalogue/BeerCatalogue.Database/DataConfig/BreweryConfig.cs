﻿using BeerCatalogue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerCatalogue.Database.DataConfig
{
    public class BreweryConfig : IEntityTypeConfiguration<Brewery>
    {
        public void Configure(EntityTypeBuilder<Brewery> builder)
        {
            builder.
                HasOne(br => br.Country)
                .WithMany(c => c.Breweries)
                .HasForeignKey(br => br.CountryId);
        }
    }
}
