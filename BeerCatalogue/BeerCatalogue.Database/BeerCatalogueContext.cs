﻿using BeerCatalogue.Database.DataConfig;
using BeerCatalogue.Database.Seed;
using BeerCatalogue.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BeerCatalogue.Database
{
    public class BeerCatalogueContext : IdentityDbContext<User, Role, int>
    {
        public BeerCatalogueContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Dranklist> Dranklists { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<Wishlist> Wishlists { get; set; }
        public DbSet<BeerRating> BeerRatings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BeerConfig());
            builder.ApplyConfiguration(new BreweryConfig());
            builder.ApplyConfiguration(new DranklistConfig());
            builder.ApplyConfiguration(new ReviewConfig());
            builder.ApplyConfiguration(new WishlistConfig());
            builder.ApplyConfiguration(new BeerRatingConfig());
            base.OnModelCreating(builder);
            builder.Seed();
        }
    }
}
