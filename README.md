# BeerCatalogue – catalogue for browsing beers that you might be interested in

### [**Link to our project board**](https://trello.com/b/XRLCl5qA/beercatalogueproject)

# Homepage
## From login page you can login ,register or just start browsing the catalogue. Dependning on whether you are guest,user or admin you can access different actions in the application.

![](./Images/Homepage.png)

# As a guest you can only look at all countries,styles,breweries,beers,beer reviews and their details 

## Beers - you can sort beers by abv,name or rating
![](./Images/Beers.png)

## Beer details
![](./Images/BeerDetailsGuest.png)

## Countries - you can view details about country and all breweries in country and beers being made in this country : by clicking more details on countries and selecting one of the options presented

![](./Images/CountryDetails.png)

## Styles - you can view details about styles and getting all beers being with that style : by clicking more details and selecting get all beers for style

![](./Images/StylesDetails.png)

# As a user you have all the advantages of a guest and now you can add new beers ,rate them,and creating reviews about them,you can even add them to wishlist if you would like to try them and dranklist if you already drank them

## Creating a beer - by pressing create new in beers you are redirected to a new page for creating a new beer that looks like
![](./Images/CreateBeer.png)

## Options for rating and creating a review for beer and adding them to wishlist or dranklist
![](./Images/BeerDetails.png)

